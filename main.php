<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'  =>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'      =>'Giraffe Agency Website',
    'language'  =>'ru',

    'preload' => array(
        'log',
    ),

	'import'=>array(
		'application.models.*',
        'application.forms.*',
		'application.components.*',
        'application.ext.shopping-cart.*',
        'application.modules.user.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.models.*',
        'application.modules.rights.components.*',
        'application.modules.review.*',
        'application.modules.review.models.*',
        'application.modules.review.components.*',
        'application.widgets.*',
	),



    'aliases'=>array(
        'vendor' => realpath(__DIR__ . '/../../vendor'),
        'backend'=> realpath(__DIR__ . '/../../backend')
    ),



	'components'=>array(

        'clientScript' => array(
            'class' => 'vendor.muayyad-alsadi.yii-EClientScript.EClientScript',
            'combineScriptFiles' => true, // By default this is set to true, set this to true if you'd like to combine the script files
            'combineCssFiles' => false, // By default this is set to true, set this to true if you'd like to combine the css files
            'optimizeScriptFiles' => true, // @since: 1.1
            'optimizeCssFiles' => true, // @since: 1.1
            'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
            'optimizeInlineCss' => false,
            'scriptMap' => array(
                'jquery.js' => '/js/jquery.min.js',
                '/js/anim-header.js' => '/js/all.js',
                '/js/vendor/EasePack.min.js' => '/js/all.js',
                '/js/vendor/TweenLite.min.js' => '/js/all.js',
            )
        ),

        'cache'=>array(
            'class'=>'system.caching.CMemCache',
            'servers'=>array(
                array('host'=>'127.0.0.1', 'port'=>11211),
            ),
        ),

        'request'=>array(
            'enableCsrfValidation'=>true,
        ),

        'user'=>array(
            'class' => 'RWebUser',
            'allowAutoLogin'=>true,
            'loginUrl'=>array('/user/login'),
        ),

        'authManager'=>array(
            'class'=>'RDbAuthManager',
            'defaultRoles' => array('Guest')
        ),

        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=> false,
            'rules'=>array(
                '' => 'site/index',
                '<action:\w+>'=>'site/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>/<id>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=gfa',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'rtyghbnm1',
			'charset' => 'utf8',
            'tablePrefix' => 'gf_',

            //For Debug...
            'enableProfiling' => true,
            'enableParamLogging' => true,
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

        'robokassa' => array(
            'class' => 'ext.yii-robokassa.Robokassa',
            'sMerchantLogin' => 'gfa.ru.id',
            'sMerchantPass1' => 'rtyghbnm1',
            'sMerchantPass2' => 'rtyghbnm2',
            'sCulture' => 'ru',
            'sIncCurrLabel' => '',
            'orderModel' => 'Invoice',
            'priceField' => 'amount',
            'isTest' => false, // тестовый либо боевой режим работы
        ),

        'amocrm'=>array(
            'class'=>'application.extensions.amocrm.amocrm',
        ),

        'shoppingCart' => array (
            'class' => 'application.ext.shopping-cart.EShoppingCart'
        ),

	),

    'modules'=>array(

        'user' => array(
            'hash' => 'md5',
            'tableUsers' => 'gf_users',
            'tableProfiles' => 'gf_profiles',
            'tableProfileFields' => 'gf_profiles_fields',
        ),
        'rights' => array(

        ),
        'review'
    ),

	'params'=>array(
		'adminEmail' => 'info@gf-a.ru',
        'sendEmail'  => 'noreply@gf-a.ru',
        'saleEmail'  => 'zakaz@gf-a.ru',
	),
);