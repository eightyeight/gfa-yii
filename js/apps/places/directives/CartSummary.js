function CartSummary(){
    return {
        restrict: 'E',
        template: '<div class="cart-inner" ng-click="order()"><img src="/images/icons/cart.svg"><div class="basket">Корзина</div><div class="cart-data"><div class="item-summary">{{cart}}</div > <span class="price-summary">{{price}} руб.</span></div></div>',
        controller: function ($scope, $location, placesCart) {
            if(placesCart.isEmpty()){
                placesCart.init();
            }
            else{
                placesCart.restore(placesCart.getStoredCart());
            }
            $scope.$watch(function(){return placesCart.getAll()},function () {
                $scope.cart = placesCart.countItems();
                $scope.price = placesCart.getTotalPrice();
            }, true);

            $scope.order = function(){
                $location.path('/order')
            }
        }
    }
}
