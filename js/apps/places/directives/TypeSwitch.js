function TypeSwitch(){

    return {
        restrict: 'EA',
        controller: 'placesAppView',
        scope: {
            chosenType: '@',
            cntrMethod: '&'
        },
        replace: true,
        link: function($scope, element, attributes){
            var options  = element.children();
            var i = 0;
            for(i; i < options.length; i++ ){
                options[i].addEventListener('click', function(e){
                    var id = e.srcElement.getAttribute('typeid');
                    var type = this.getAttribute('typeid');
                    $scope.chosenType = type;
                    if(this.className == "choice"){
                        this.classList.add('on');
                        if(id == 1)
                            options[2].classList.remove('on');
                        else
                            options[1].classList.remove('on');
                    }

                })
            }
        },
        template: '<div class="switcher" chosen-type="1" cntr-method="chosen(id)" ng-click="cntrMethod({id: chosenType})"><div class="pointer"></div><div class="choice" typeid="1">Статика</div><div class="choice on" typeid="0">Динамика</div></div>'
    }

}