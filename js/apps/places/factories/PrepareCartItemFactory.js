
function PrepareCartItemFactory(){
    var item = function(id, title, description, image, type, price, count){
        this.setId(id);
        this.setTitle(title);
        this.setDescription(description);
        this.setImage(image);
        this.setType(type);
        this.setPrice(price);
        this.setCount(count);
    };

    item.prototype.setId = function(id){
        if(id)
            this.id = id;
    };

    item.prototype.getId = function (){
        return this.id;
    };

    item.prototype.setTitle = function (title){
        if(title)
            this.title = title;
    };

    item.prototype.getTitle = function(){
        return this.title;
    };

    item.prototype.setDescription = function(description){
        if(description)
            this.description = description;
    };

    item.prototype.getDescription = function(){
        return this.description;
    };

    item.prototype.setImage = function(image){
        if(image)
            this.image = image;
    };

    item.prototype.getImage = function(){
        return this.image;
    }

    item.prototype.setType = function(type){
        if(type)
            this.type = type;
    }

    item.prototype.getType = function(){
        return this.type;
    }

    item.prototype.setPrice = function(price){
        if(price)
            this.price = price;
    };

    item.prototype.getPrice = function () {
        return this.price;
    };

    item.prototype.setCount = function (count) {
        if(count){
            var countInt = parseInt(count);
            if(this.count)
                this.count += countInt;
            else
                this.count = countInt;
        }
    };

    item.prototype.getCount = function () {
        return this.count;
    };

    item.prototype.toObject = function () {
        return {
            id: this.getId(),
            title: this.getTitle(),
            description: this.getDescription(),
            image: this.getImage(),
            type: this.getType(),
            price: this.getPrice(),
            count: this.getCount()
        }
    };

    return item;
}
