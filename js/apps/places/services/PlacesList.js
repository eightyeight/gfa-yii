/**
 * Created by web on 26.12.14.
 */
function PlacesList($http){

    /**
    * @ToDo better to initialize like a factory not service;
    */

    var markers = [];

    $http.get('/places/places/list')
    .success(function(data, status, headers, config) {
        places = data;
        _.each(places, function (place) {
            place.options = {
                icon: {
                    url: "" + place.icon,
                    scaledSize: {width: 20, height: 35, D: "px", I: "px"}
                }
            };
            place.onClicked = function () {
                place.showWindow = true;
                $scope.$apply();
            };
            delete place.icon;

            markers.push(place);
        });
    })
    .error(function(){
        console.log('error');
    })

    return {
        list: markers
    }

}