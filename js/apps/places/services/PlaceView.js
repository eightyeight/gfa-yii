/**
 * Created by web on 26.12.14.
 */
function PlaceView($http){

    this.getPlace = function(placeId, csrf){

        var promise = $http({
                method: 'POST',
                url: '/places/places/view',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {YII_CSRF_TOKEN: csrf, id: placeId}
            });

        return promise;
    }

}



