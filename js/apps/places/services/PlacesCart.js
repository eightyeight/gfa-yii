
function PlacesCart(localStorageService, prepareItem){

    this.init = function(){
        this.cart = {
            items: []
        }
    };
    this.getCart = function(){
        return this.cart;
    };
    this.setCart = function(cart){
        if(cart)
            this.cart = cart;
    };

    this.save = function(){
        localStorageService.set('gfCart', this.getCart());
    };

    this.existenceCheck = function (itemId, typeName){
        for(var i = 0; i < this.cart.items.length; i++){
            if(this.cart.items[i].id == itemId && this.cart.items[i].type == typeName){
                this.cart.items[i].count++;
                return true;
            }
        }
        return false;
    };

    this.addItem = function (newItem){
        if(!this.existenceCheck(newItem.id, newItem.type)){
            this.cart.items.push(newItem);
        }
        this.save();
    };

    this.getAll = function(){
        var cartItems = this.getCart().items;
        return cartItems;
    };

    this.countItems = function(){
        return this.cart.items.length;
    };

    this.getTotalPrice = function(){
        var totalPrice = 0;

        for(var i = 0; i < this.cart.items.length; i++){
            totalPrice += parseInt(this.cart.items[i].price);
        }

        return totalPrice;
    };

    this.restore = function(storedCart){
        var _self = this;
        _self.init()

        angular.forEach(storedCart.items, function(item){
           _self.cart.items.push(new prepareItem(item.id, item.title, item.description, item.image, item.type, item.price, item.count));
        });
        _self.save();
    };

    this.getItem = function(id, type){
        for(var i = 0; i < this.cart.items.length; i++){
            if(this.cart.items[i].id == id && this.cart.items[i].type == type){
                return items[i];
            }
        }
    };

    this.deleteItem = function(id, type){
        for(var i = 0; i < this.cart.items.length; i++){
            if(this.cart.items[i].id == id && this.cart.items[i].type == type){
                this.cart.items.splice(i,1);
            }
        }
        console.log('delete');
    }

    this.isEmpty = function(){
        if(localStorageService.get('gfCart') instanceof Object){
            return false;
        }
        else{
            return true;
        }
    };

    this.getStoredCart = function(){
        if(localStorageService.get('gfCart') instanceof Object){
            return localStorageService.get('gfCart');
        }
    };

    this.serialize = function(){
        var itemsArray = [];
        for(var i = 0; i < this.cart.items.length; i++){
            var itemObj = {};
            itemObj.title       = this.cart.items[i].getTitle();
            itemObj.type        = this.cart.items[i].getType();
            itemObj.price       = this.cart.items[i].getPrice();
            itemObj.description = this.cart.items[i].getDescription();
            itemsArray.push(itemObj);
        }
        return itemsArray;
    }

    this.free = function(){
        localStorageService.clearAll();
        this.init();
    }

}
