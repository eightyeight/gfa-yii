
angular.module('placesApp',['uiGmapgoogle-maps', 'LocalStorageModule', 'ngRoute','angular-loading-bar', 'ngAnimate'])
    .config(function(uiGmapGoogleMapApiProvider,localStorageServiceProvider, $routeProvider, $locationProvider) {

    /**
     * Configure GoogleMapsProvider
     */

    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        language: 'ru',
        libraries: ''
    });

    /**
     * Configure localStorageProvider
     */

    localStorageServiceProvider
        .setStorageType('sessionStorage')
        .setPrefix('');

    /**
     * Configure routingProvider
     */

    $routeProvider
        .when('/', {
            controller: 'placesAppList'
        })
        .when('/view/:placeId', {
            controller:'placesAppView',
            templateUrl:'/places/places/viewpartial'
        })
        .when('/order', {
            controller: 'placesAppOrder',
            templateUrl: '/places/order/index'
        })
        .when('/order/info', {
            controller: 'placesAppOrderInfo',
            templateUrl: '/places/order/info'
        })
        .otherwise({
            redirectTo: '/'
        });
    })

    /**
    * Controllers
    */

    .controller('placesAppList', AppList)

    .controller('placesAppOrder', AppOrder)

    .controller('placesAppView', AppView)

    .controller('placesAppOrderInfo', AppOrderInfo)

    /**
    * Services
    */

    .service('placesList', PlacesList)

    .service('placeView', PlaceView)

    .service('placesCart', PlacesCart)

    /**
    * Factories
    */

    .factory('prepareItem', PrepareCartItemFactory)

    /**
    * Directives
    */

    .directive('summary', CartSummary)

    .directive('switcher', TypeSwitch)

    .directive('cart', CartView)
;
