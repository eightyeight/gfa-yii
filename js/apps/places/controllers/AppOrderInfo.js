/**
 * Created by web on 29.01.15.
 */
function AppOrderInfo($scope, $http, localStorageService, placesCart){

    this.checkCart(placesCart);
    var csrf = this.getCsrf(localStorageService);


    $scope.cart = placesCart.getCart();
    $scope.totalPrice = placesCart.getTotalPrice();
    $scope.makeOrder = function(isValid){
        if(isValid){
            var data = {
                customer:{
                    name: $scope.customer.name.$modelValue,
                    email: $scope.customer.email.$modelValue,
                    phone: $scope.customer.phone.$modelValue
                },
                items: $scope.cart.items,
                total: $scope.totalPrice
            };
            $scope.save($http, placesCart, angular.toJson(data), csrf);
        }
    };


    $scope.save = function($http, placesCart, data, csrf){
        $http({
            method: 'POST',
            url: '/places/order/make',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {YII_CSRF_TOKEN: csrf, data: data}
        })
        .success(function(data, status, headers, config){
           if(data == 'success'){
               placesCart.free();
           }

            $('.orderInfoWrapper').html();
        })
        .error(function(data, status, headers, config){
            console.log('error');
        });
    };
};

AppOrderInfo.prototype.checkCart = function(placesCart){
    if(placesCart.isEmpty()){
        placesCart.init();
    }
    else{
        placesCart.restore(placesCart.getStoredCart());
    }
};

AppOrderInfo.prototype.getCsrf = function(localStorageService){
    if(localStorageService.cookie.isSupported) {
        var csrfToken = localStorageService.cookie.get('YII_CSRF_TOKEN');
    }
    return csrfToken;
};


