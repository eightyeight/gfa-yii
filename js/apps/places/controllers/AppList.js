/**
 * Created by web on 25.12.14.
 */

function AppList ($scope, $location, uiGmapGoogleMapApi, placesList )
{
    jQuery('.placeOrderWrapper').hide();


    uiGmapGoogleMapApi.then(function() {

        $scope.map = {
            center: {latitude:  56.185308, longitude:  36.982914},
            zoom: 16
        };

        $scope.options = {
            disableDefaultUI: true
        };

        $scope.events = {
            click: function(marker){
                $location.path('/view/' + marker.key);
            }
        };

        $scope.placesModels = placesList.list;
    });
}