/**
 * Created by web on 25.12.14.
 */
function AppOrder($scope, $location, placesCart){

    this.checkCart(placesCart);

    $scope.cart = placesCart.getCart();
    $scope.deleteItem = function(item){
      placesCart.deleteItem(item.id, item.type);
      $scope.cart = placesCart.getCart();
      placesCart.save()
    };
    $scope.makeOrder = function(){
        $location.path('/order/info');
    };
    $scope.cancelOrder = function(){
        $location.path('/');
    };

}

AppOrder.prototype.checkCart = function(placesCart){
    if(placesCart.isEmpty()){
        placesCart.init();
        console.log('init');
    }
    else{
        placesCart.restore(placesCart.getStoredCart());
        console.log('restore');
    }
}

