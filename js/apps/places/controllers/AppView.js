/**
 * Created by web on 25.12.14.
 */
function AppView($scope, $routeParams, $location, localStorageService, placeView, placesCart, prepareItem){

    if(placesCart.isEmpty()){
        placesCart.init();
    }
    else{
        placesCart.restore(placesCart.getStoredCart());
    }

    var place;

    var placeId = $routeParams.placeId;

    if(localStorageService.cookie.isSupported) {
        var csrfToken = localStorageService.cookie.get('YII_CSRF_TOKEN');
    }

    $scope.isVisible = true;

    $scope.$on('$routeChangeSuccess', function () {
        placeView.getPlace(placeId, csrfToken).then(function (response) {
            place = response.data;
            $scope.place = place;
            $scope.selectedType = place.ad_types[0];

            if(place.ad_types.length > 1)
                $scope.countTypes = true;
            else
                $scope.countTypes = false;
        });
    });

    $scope.addToCart = function(){
        var place = $scope.place;
        var type  = $scope.selectedType;
        var item, count;
        count = 1;
        item = new prepareItem(place.id, place.title, place.description, place.image, type.name, type.price, count);
        placesCart.addItem(item);
    };

    $scope.hideView = function(){
        $location.path('/');
    };

    $scope.chosen = function(id){
        $scope.selectedType = $scope.place.ad_types[id];
        $scope.$apply;
    };


}