/**
 * Created by web on 24.11.14.
 */

    function menuFixedHoverEffect(el) {
        this.el = $(el);
        this.childs = this.el.children('.navItem');
        var activeItem;



        if($(el +' > .active').length > 0)
            activeItem = this.el.children('.active');
        else
            activeItem = $('.active').parents('.navItem');

        var leftPos = activeItem.offset().left;
        var width   = activeItem.outerWidth();
        var startPos  = leftPos + Math.round(width / 2);
        var pointer = this.el.children('#pointer');
        pointer.css({left: startPos +'px'})
        console.log(activeItem.offset().left + '+' + Math.round(activeItem.outerWidth() / 2) + '=' + startPos);
        this.menupointer = new menuPointer(pointer);

    }

    menuFixedHoverEffect.prototype.run = function(){
        var menuPointer = this.menupointer;

        this.childs.each(
            function(){
                $(this).hover(
                    function(){
                        var leftPos = $(this).offset().left;
                        var width   = $(this).outerWidth();
                        var position  = leftPos + Math.round(width / 2);
                        console.log(leftPos + '+' + Math.round(width / 2) + '=' + position);
                        menuPointer.moveTo(position);
                    },
                    function(){
                        menuPointer.moveStart();
                    }
                );
            },
            menuPointer
        )
    }


    function menuPointer(el){
        this.el = $(el);
    }

    menuPointer.prototype.moveTo = function(position){
        this.el.css({left: position + "px"});
    }

    menuPointer.prototype.moveStart = function(){
        this.el.css({left: this.startPos + "px"});
    }

    function Carusel ( el ) {
        this.element = el;
        this.scale = '.8';
        this.step = 0;
        this.slides = this.element.children;
    }
