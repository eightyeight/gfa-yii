/**
 * Created by web on 08.08.14.
 *
 *
 */
    $(document).ready(function(){

        var element = document.getElementById('top-slider');
        var carusel = new Carusel(element);
        carusel.init();
        setTimeout(function(){carusel.run()}, 500);
        setInterval(function(){carusel.run()},5000);


        $('.call-button').click(function(){
            if(!$('.tooltip').hasClass('active')) {
                $('.tooltip').fadeIn('clip').addClass('active');
            }
            else{
                $('.tooltip').fadeOut('clip').removeClass('active');
            }
        });

        $('#phone').inputmask("mask", {"mask": "+7 (999) 999-9999"});

        $(document).mouseup(function (e)
        {
            var container = $(".tooltip");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.fadeOut('clip').removeClass('active');
            }
        });

        $('.bxslider').bxSlider({
            slideWidth: 750,
            slideMargin: 50,
            infiniteLoop: false,
            adaptiveHeight: true,
            responsive: true,
            slideSelector: '#cl-slide',
            pager: false,
            nextSelector: '.slider-next',
            prevSelector: '.slider-prev',
            nextText: '<img src="images/controls-rg.png">',
            prevText: '<img src="images/controls-lf.png">'
        });

        var container = $('#services');
        serviceAnimate(container);
        $('.pointer').hover(function(){return false;},function(){return false;});

        var menuAnimation = new menuFixedHoverEffect('.navInner');

        menuAnimation.run();
    });
    /*
    * End ready.
    */

     $(window).on('scroll', function(){

        if($(window).scrollTop() > 142){
            $('#nav').addClass('fixed');
            $('.alt-logo').hide();
        }
        else{
            $('#nav').removeClass('fixed');
            $('.alt-logo').show();
        }
    });

    function serviceAnimate($elem){
        $elem.children().each(function(){
            var index = $(this).index();
            var $this = $(this);
            var delay = 250 * index;

            setTimeout(function(){
                $this.show().addClass('fadeInUp animated');
            }, delay);

        });
    }

    function menuFixedHoverEffect(el) {
        this.el = $(el);
        this.childs = this.el.children('.navItem');

        var pointer = this.el.children('#pointer');
        var startPos = this.el.children('.active').position().left;
        this.menupointer = new menuPointer(pointer, startPos);

    }

    menuFixedHoverEffect.prototype.run = function(){
       var menuPointer = this.menupointer;

       this.childs.each(
           function(){
              $(this).hover(
                  function(){

                     var leftPos = $(this).offset().left;
                     var width   = $(this).outerWidth();
                     var position  = leftPos + Math.round(width / 2);
                     menuPointer.moveTo(position);
                  },
                  function(){
                     menuPointer.moveStart();
                  }
              );
           },
           menuPointer
       )
    }


    function menuPointer(el, startPos){
        this.el = $(el);
        this.startPos = startPos;
        this.el.css({left: this.startPos + "px"});
    }

    menuPointer.prototype.moveTo = function(position){
        this.el.css({left: position + "px"});
    }

    menuPointer.prototype.moveStart = function(){
        this.el.css({left: this.startPos + "px"});
    }

    function Carusel ( el ) {
        this.element = el;
        this.scale = '.8';
        this.step = 0;
        this.slides = this.element.children;
    }

    Carusel.prototype.init = function(){

        var  slideEl;

        for(childIndex in this.slides) {
            slideEl = this.slides[childIndex];

            slideEl.style.display = 'none';

            if(childIndex >= 2) break;
        }
    }

    Carusel.prototype.hasClass = function(target, className){
            return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
    }

    Carusel.prototype.run = function(){

        var slides;

        slides = this.element.children;
        if(this.hasClass(slides[this.step],'fadeInRight'))
            slides[this.step].classList.remove('fadeInRight');
        slides[this.step].className += ' fadeOutUp';

        if(this.step == 2)
            this.step = 0;
        else
            this.step = this.step + 1;
        if(this.hasClass(slides[this.step],'fadeOutUp'))
            slides[this.step].classList.remove('fadeOutUp');
        if(!this.hasClass(slides[this.step],'animated'))
            slides[this.step].className += ' animated';
        this.slides[this.step].style.display = 'block';
        this.slides[this.step].className += ' fadeInRight';

    }

    //Do Nothing, placed here for future usages

    function init(){


    }

    window.addEventListener( 'DOMContentLoaded', init, false);