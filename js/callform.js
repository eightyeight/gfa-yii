/**
 * Created by web on 01.12.14.
 */

$(function(){
    $('#call-button').on('click', function(){

        $('.popup').dialog({
            modal: true,
            width: 400,
            position: { my: "100px", at: "100px", of: window },
            closeText: ''

        })

        $('.ui-icon-closethick').addClass('fa fa-times');
        return false;
    });

});