<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'  =>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'      =>'Giraffe Agency Website',
    'language'  =>'ru',

    'preload' => array(
        'log',
    ),

	'import'=>array(
		'application.models.*',
        'application.forms.*',
		'application.components.*',
        'application.ext.shopping-cart.*',
        'application.modules.user.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.models.*',
        'application.modules.rights.components.*',
        'application.modules.review.*',
        'application.modules.review.models.*',
        'application.modules.review.components.*',
        'application.widgets.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        'ext.shopping-cart.*'
	),



    'aliases'=>array(
        'vendor' => realpath(__DIR__ . '/../../vendor'),
        'backend'=> realpath(__DIR__ . '/../../backend')
    ),



	'components'=>array(

        'clientScript' => array(
            'class' => 'vendor.muayyad-alsadi.yii-EClientScript.EClientScript',
            'combineScriptFiles' => true, // By default this is set to true, set this to true if you'd like to combine the script files
            'combineCssFiles' => false, // By default this is set to true, set this to true if you'd like to combine the css files
            'optimizeScriptFiles' => true, // @since: 1.1
            'optimizeCssFiles' => true, // @since: 1.1
            'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
            'optimizeInlineCss' => false,
            'scriptMap' => array(
                'jquery.js' => '/js/jquery.min.js',
                '/js/anim-header.js' => 'all.js',
                '/js/vendor/EasePack.min.js' => 'all.js',
                '/js/vendor/TweenLite.min.js' => 'all.js',

            )
        ),

        /**
         * Uncomment when memcached install.
         */

//        'cache'=>array(
//            'class'=>'system.caching.CMemCache',
//            'servers'=>array(
//                array('host'=>'127.0.0.1', 'port'=>11211),
//            ),
//        ),

        'request'=>array(
            'enableCsrfValidation'=>true,
        ),

        'user'=>array(
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl'=>array('/user/login'),
        ),

        'authManager'=>array(
            'class'=>'RDbAuthManager',
            'defaultRoles' => array('Authenticated','Guest'),
            'itemTable'=>'gf_authItem',
            'assignmentTable'=>'gf_auth_assignment',
            'itemChildTable'=>'gf_authItemChild'
        ),

        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=> false,
            'rules'=>array(
                '' => 'site/index',
                '<action:\w+>'=>'site/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>/<id>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=gfa',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'rtyghbnm',
			'charset' => 'utf8',
            'tablePrefix' => 'gf_',

            //For Debug...
            'enableProfiling' => true,
            'enableParamLogging' => true,
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

        'robokassa' => array(
            'class' => 'ext.yii-robokassa.Robokassa',
            'sMerchantLogin' => 'gfa.ru.id',
            'sMerchantPass1' => 'rtyghbnm1',
            'sMerchantPass2' => 'rtyghbnm2',
            'sCulture' => 'ru',
            'sIncCurrLabel' => '',
            'orderModel' => 'Invoice',
            'priceField' => 'amount',
            'isTest' => false, // тестовый либо боевой режим работы
        ),

        'amocrm'=>array(
            'class'=>'application.extensions.amocrm.amocrm',
        ),

        'shoppingCart' => array (
            'class' => 'ext.shopping-cart.EShoppingCart'
        ),

        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'services' => array( // You can change the providers and their classes.

                'facebook' => array(
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'FacebookOAuthService',
                    'client_id' => '733408680084078',
                    'client_secret' => 'fe0337cbccfa6f45f9c6977d1a2c2ed7',
                ),

                'vkontakte' => array(
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'VKontakteOAuthService',
                    'client_id' => '4662420',
                    'client_secret' => 'Asc7vHVZASbtABywNf3Z',
                ),

            ),
        ),

	),

    'modules'=>array(

        'user' => array(
            'hash' => 'md5',
            'tableUsers' => 'gf_users',
            'tableProfiles' => 'gf_profiles',
            'tableProfileFields' => 'gf_profiles_fields',
        ),
        'rights' => array(

        ),
        'review',
        'places',
    ),

	'params'=>array(
		'adminEmail' => 'info@gf-a.ru',
        'sendEmail'  => 'noreply@gf-a.ru',
        'saleEmail'  => 'zakaz@gf-a.ru',
	),
);