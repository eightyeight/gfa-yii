<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 12.11.14
 * Time: 12:02
 */



class Review extends CActiveRecord{

    const NOT_APPROVED = 0;
    const APPROVED = 1;
    const ARCHIVED = 2;

    public function tableName(){
        return 'gf_review';
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_time',
                'updateAttribute' => 'update_time',
            )
        );
    }

    public function scopes(){
        return array(
            'approved' => array(
                'condition' => 'r_status=' . self::APPROVED
            ),
        );
    }

    protected function beforeSave(){
        if($this->isNewRecord)
            $this->r_status = 0;
        return parent::beforeSave();
    }

    public function relations()
    {
        Yii::import('application.modules.user.models.*');
        return array(
            'userCreate' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function rules(){
        return array(
            array('user_email', 'email'),
            array('user_name, user_email, review_text','required'),
            array('user_name,user_email,create_time,update_time,review_text,work_position,company_name,r_status, user_id','safe')
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_name' => 'User Name',
            'user_email' => 'User Email',
            'created_time' => 'Time created at',
            'update_time' => 'Time updated at',
            'review_text' => 'Review text',
            'work_position' => 'Work Position',
            'company_name'  => 'Company Name',
            'r_status' => 'Review Status'
        );
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
} 