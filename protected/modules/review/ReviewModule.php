<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 12.11.14
 * Time: 11:22
 */




class ReviewModule extends CWebModule{

    public $defaultController = "review";

    public $layout = 'application.views.layouts.site';

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
            'review.models.*',
            'review.components.*',
            'review.controllers.*',
            'review.widgets.*'
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * @param $str
     * @param $params
     * @param $dic
     * @return string
     */
    public static function t($str='',$params=array(),$dic='news') {
        if (Yii::t("ReviewModule", $str)==$str)
            return Yii::t("ReviewModule.".$dic, $str, $params);
        else
            return Yii::t("ReviewModule", $str, $params);
    }
} 