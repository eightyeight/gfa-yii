<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 12.11.14
 * Time: 11:24
 */



class ReviewController extends CController {

    public function actionCreate(){
        if(Yii::app()->request->isAjaxrequest){

            $model = new Review;
            $model->attributes = Yii::app()->request->getPost('Review');
            if($model->validate()){
                if($model->save()){
                    echo CJSON::encode(
                        array('status' => 'success')
                    );
                }
            }
            else{
                $errors = $model->getErrors();

                echo CJSON::encode($errors);

            }
        }
    }

    public function actionList(){
        if(Yii::app()->request->isAjaxrequest)
                $this->widget('ReviewListWidget');
    }

    public function actionTest(){

    }

} 