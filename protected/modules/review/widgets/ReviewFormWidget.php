<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 13.11.14
 * Time: 14:55
 */

class ReviewFormWidget extends CWidget {

    public $model;

    public function init(){
        if(!$this->model)
            $this->model = new Review;
    }

    public function run(){

        $this->render('form', array('model' => $this->model));

    }
} 