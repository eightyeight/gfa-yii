<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 13.11.14
 * Time: 14:34
 */




class ReviewListWidget extends CWidget{

    public $data;

    public function init(){
        if(!$this->data){
            $this->data = Review::model()
                            ->approved()
                            ->with('userCreate')
                            ->findAll();
        }

        parent::init();
    }

    public function run(){

       $this->render('list', array('data' => $this->data));

    }


} 