<?php


    foreach($data as $review):
?>
        <div class="single-review">
            <header>
                <div>
                    <img src="<?php echo $review->userCreate->profile->userpic;?>">
                </div>

                <span><?php echo $review->user_name;?></span>
                <?php if(isset($review->work_position)):?>
                    <span><?php echo $review->work_position?></span>
                <?php endif;?>

                <?php if(isset($review->company_name)):?>
                    <span><?php echo $review->company_name?></span>
                <?php endif;?>
            </header>
            <div class="text">
                <?php echo $review->review_text;?>
            </div>

        </div>
<?php
    endforeach;
?>
