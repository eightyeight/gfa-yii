<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 13.11.14
 * Time: 14:57
 */

echo CHtml::openTag('div', array(
    'class' => 'form',
));
    $form=$this->beginWidget('CActiveForm', array(
        'action'=> '',
        'id'=> 'review_form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'action'=> CHtml::normalizeUrl(array('review/review/create')),
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>true,
        ),
    ));

    echo $form->error($model, 'user_name');
    echo $form->textField($model, 'user_name');

    echo $form->error($model, 'user_email');
    echo $form->textField($model, 'user_email');


    echo $form->error($model, 'review_text');
    echo $form->textArea($model, 'review_text');

    echo $form->hiddenField($model, 'user_id', array('value' => Yii::app()->user->id));


    echo Chtml::ajaxsubmitButton('Написать',Yii::app()->createUrl('review/review/create'), array(
            'type'=>'POST',
            'success'=>'function(data){
                console.log(data);
                data = jQuery.parseJSON(data);
                if(data.status=="success"){
                    jQuery("#sendAjax").attr("disabled", true);
                    try{
                       jQuery.ajax({
                          type: "post",
                          data: "YII_CSRF_TOKEN='.Yii::app()->request->csrfToken.'",
                          url : "'.Yii::app()->createUrl('review/review/list').'",
                          success: function(data){
                            $(".reviews").add(data);
                            console.log(data);
                          }
                       });
                    }
                    catch(e){
                        console.log(e.getMessage);
                    }
                }
                else{
                    jQuery.each(data, function(key, value) {
                        jQuery("#Review_"+key+"_em_").show().html(value.toString());
                    });
                }
            }'
        ),
        array(
            'id' => 'sendAjax',
            'class' => 'button special'
        )
       );



    $this->endWidget();


echo Chtml::closeTag('div');