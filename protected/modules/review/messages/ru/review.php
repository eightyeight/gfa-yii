<?php

return array(
    'User Name' => 'Ваше Имя',
    'User Email' => 'Ваш Email',
    'Time created at' => 'Время создания',
    'Time updated at' => 'Время изменения',
    'Review text' => 'Текст отзыва',
    'Work Position' => 'Должность',
    'Company Name' => 'Компания'
);

