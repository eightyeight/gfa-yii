<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 24.12.14
 * Time: 11:33
 */
Yii::import('vendor.swiftmailer.swiftmailer.lib.swift_required', true);

class OrderController extends FrontendController
{
    public function actionIndex(){
        $this->renderPartial('index');
    }

    public function actionView(){
        $this->renderPartial('view');
    }

    public function actionInfo() {
        $this->renderPartial('info');
    }

    public function actionMake(){

        $data = json_decode($_POST['data']);

        $customer  = $data->customer;
        $positions = $data->items;
        $total     = $data->total;

        $order = new Order();
        $order->customer_name  = $customer->name;
        $order->customer_email = $customer->email;
        $order->customer_phone = $customer->phone;
        $order->order_total    = $total;

        if($order->save()){
            if($this->savePositions($positions, $order->id)){
                echo 'success';
                Yii::app()->end();
            }
        }

    }

    public function actionSend()
    {
        $model = new PlaceOrderForm();

        $model->attributes = Yii::app()->request->getPost('PlaceOrderForm');
        if ($model->validate()) {
            try {
                if ($this->sendHtmlMail($model)) {
                    echo CJSON::encode(array(
                        'status' => 'success'
                    ));
                }
                Yii::app()->end();
            } catch (Exception $e) {
                Yii::log('Error msg: ' . $e->getMessage(), 'error');
            }
        } else {
            echo CJSON::encode(array(
                'status' => 'error'
            ));
            Yii::app()->end();
        }
    }

    public function sendHtmlMail(CFormModel $model)
    {


        $subject = Yii::app()->request->hostinfo . ': Получен новый лид.';

        $body = '<html>' .
            ' <head></head>' .
            '<body>' .
            '<p>
            Здравствуйте
            </p>

            <p>
            Получен новый лид с целевой страницы: ' . Yii::app()->request->hostinfo . '
				</p>

				<p>
				Данные лида:
				</p>
                <p>
                Cайт: "' . Yii::app()->request->hostinfo . '"
                </p>


				<p>
				Имя: "' . $model->name . '"
				</p>

				<p>
				Телефон: "' . $model->phone . '"
                </p>

				<p>
				Почта: "' . $model->email . '"
				</p>

				<p>
				--
				Это автоматическое сообщение, отвечать на него не нужно.
				</p>
				' .
            '</body>' .
            '</html>';

        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('noreply@gf-a.ru')
            ->setTo('bodrov@gf-a.ru')
            ->setBody($body, 'text/html');


        $transport = Swift_SmtpTransport::newInstance();

        $mailer = Swift_Mailer::newInstance($transport);

        return $mailer->send($message);
    }


    public function savePositions($positions, $orderId){
        $valuesArr = [];
        foreach ($positions as $position) {
            $valuesArr[] = array(
                'place_id'    => $position->id,
                'order_id'    => $orderId,
                'title'       => $position->title,
                'description' => $position->description,
                'image'       => $position->image,
                'type'        => $position->type,
                'price'       => $position->price
            );
        }

        $builder=Yii::app()->db->schema->commandBuilder;
        $command=$builder->createMultipleInsertCommand('gf_order_position', $valuesArr);
        if($command->execute())
            return true;
        else
            return false;
    }


} 