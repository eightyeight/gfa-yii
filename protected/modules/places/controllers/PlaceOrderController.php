<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 24.12.14
 * Time: 11:33
 */
Yii::import('vendor.swiftmailer.swiftmailer.lib.swift_required', true);
class PlaceOrderController extends FrontendController{

    public function actionSend(){
      $model = new PlaceOrderForm();

      $model->attributes = Yii::app()->request->getPost('PlaceOrderForm');
      if($model->validate()){
        try{
          if($this->sendHtmlMail($model)){
              echo CJSON::encode(array(
                  'status'=>'success'
              ));
          }
          Yii::app()->end();
        }
        catch(Exception $e){
            Yii::log('Error msg: '. $e->getMessage(), 'error');
        }
      }
      else{
          echo CJSON::encode(array(
              'status' => 'error'
          ));
          Yii::app()->end();
      }
    }

    public function sendHtmlMail(CFormModel $model) {



        $subject = Yii::app()->request->hostinfo .': Получен новый лид.';

        $body = '<html>' .
            ' <head></head>' .
            '<body>'.
            '<p>
            Здравствуйте
            </p>

            <p>
            Получен новый лид с целевой страницы: '. Yii::app()->request->hostinfo .'
				</p>

				<p>
				Данные лида:
				</p>
                <p>
                Cайт: "'.Yii::app()->request->hostinfo.'"
                </p>


				<p>
				Имя: "'. $model->name .'"
				</p>

				<p>
				Телефон: "'. $model->phone .'"
                </p>

				<p>
				Почта: "' . $model->email . '"
				</p>

				<p>
				--
				Это автоматическое сообщение, отвечать на него не нужно.
				</p>
				'.
            '</body>' .
            '</html>';

        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('noreply@gf-a.ru')
            ->setTo('bodrov@gf-a.ru')
            ->setBody($body, 'text/html');



        $transport = Swift_SmtpTransport::newInstance();

        $mailer = Swift_Mailer::newInstance($transport);

        return $mailer->send($message);
    }
} 