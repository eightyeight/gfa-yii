<?php

/**
 * Created by PhpStorm.
 * User: web
 * Date: 15.12.14
 * Time: 18:14
 */
class PlacesController extends FrontendController
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionList()
    {
        $data = Places::model()->with('type')->findAll();
        echo CJSON::encode($this->prepareResponceData($data));
        Yii::app()->end();
    }

    private function prepareResponceData($data)
    {

        $responceArray = array();

        foreach ($data as $place) {
            $responceArray[] = array(
                'id' => $place->id,
                'latitude' => $place->p_lat,
                'longitude' => $place->p_lng,
                'icon' => $place->type[0]->icon
            );
        }

        return $responceArray;
    }

    public function actionView()
    {
        $id = Yii::app()->request->getPost('id');
        $data = Places::model()->with('type')->findByPk($id);
        echo CJSON::encode($this->prepareViewData($data));
        Yii::app()->end();

    }

    private function prepareViewData($data)
    {
        $responseArray = array(
            'id' => $data->id,
            'title' => $data->p_title,
            'description' => $data->p_description,
            'ad_types' => $data->type,
            'image' => $data->p_image
        );
        return $responseArray;
    }

    public function actionViewPartial()
    {
        $this->renderPartial('view');
        Yii::app()->end();
    }

    public function actionFormPartial()
    {
        $model = new PlaceOrderForm();
        $this->renderPartial('form', array('model' => $model));
        Yii::app()->end();
    }
} 