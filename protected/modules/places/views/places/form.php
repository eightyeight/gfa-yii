<div>
    <div class="cancelOrder"><i class="fa fa-times fa-2x" ng-click="cancelOrder()"></i></div>
    <div>{{type.name}}</div>
    <img src="{{place.image}}" class="pFormImage">

    <div>Цена за месяц: {{type.price}}</div>
    <?php $oform = $this->beginWidget('CActiveForm', array(
        'id' => 'order-form',
        'action' => '',
    ));?>

    <input ng-value="place.description" name="PlaceOrderForm[placename]" id="PlaceOrderForm_price" type="hidden">
    <input ng-value="place.type.name" name="PlaceOrderForm[placeType]" id="PlaceOrderForm_price" type="hidden">
    <input ng-value="place.type.price" name="PlaceOrderForm[price]" id="PlaceOrderForm_price" type="hidden">

    <?php echo $oform->textField($model, 'name', array('placeholder' => 'Введите Имя', 'class' => 'promo-input', 'id' => 'name')); ?>
    <?php echo $oform->error($model, 'name', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;')); ?>
    <?php echo $oform->textField($model, 'phone', array('placeholder' => 'Введите Телефон', 'class' => 'promo-input', 'id' => 'phone')); ?>
    <?php echo $oform->error($model, 'phone', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;')); ?>
    <?php echo $oform->textField($model, 'email', array('placeholder' => 'Введите E-mail', 'class' => 'promo-input', 'id' => 'phone')); ?>
    <?php echo $oform->error($model, 'email', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;')); ?>
    <?php $this->endWidget(); ?>
    <button ng-click="sendOrder()" class="cartAdd">
        Готово!
    </button>
</div>