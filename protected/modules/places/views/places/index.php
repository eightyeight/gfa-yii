<section id="placesApp" class="placesAppWrapper" ng-app="placesApp">

    <summary class="cart"></summary>

    <div id="map_canvas" ng-controller="placesAppList" class="placesMapWrapper">
        <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
            <ui-gmap-markers models="placesModels" coords="'self'" ng-click="'onClicked()'" options="'options'"
                             events="events">
<!--                <ui-gmap-windows show="'showWindow'" closeClick="'closeClick'" ng-cloak>-->
<!--                    <p ng-non-bindable>This is an info window at {{ latitude | number:4 }}, {{ longitude | number:4 }}!</p>-->
<!--                </ui-gmap-windows>-->
            </ui-gmap-markers>
        </ui-gmap-google-map>
    </div>
        <div ng-view></div>
    </div>

    <div style="clear: both"></div>
</section>
<script src="/js/vendor/lodash/lodash.js"></script>
<script src="/js/vendor/angular/angular.js"></script>
<script src="/js/vendor/angular-animate/angular-animate.js"></script>
<script src="/js/vendor/angular-route/angular-route.js"></script>
<script src="/js/vendor/angular-local-storage/angular-local-storage.js"></script>
<script src="/js/vendor/angular-google-maps/angular-google-maps.js"></script>
<script src="/js/vendor/angular-loading-bar/loading-bar.js"></script>
<script src="/js/apps/places/controllers/AppList.js"></script>
<script src="/js/apps/places/controllers/AppOrder.js"></script>
<script src="/js/apps/places/controllers/AppView.js"></script>
<script src="/js/apps/places/controllers/AppOrderInfo.js"></script>
<script src="/js/apps/places/services/PlacesList.js"></script>
<script src="/js/apps/places/services/PlaceView.js"></script>
<script src="/js/apps/places/services/PlacesCart.js"></script>
<script src="/js/apps/places/factories/PrepareCartItemFactory.js"></script>
<script src="/js/apps/places/directives/CartSummary.js"></script>
<script src="/js/apps/places/directives/TypeSwitch.js"></script>
<script src="/js/apps/places/directives/CartView.js"></script>
<script src="/js/apps/places/placesApp.js"></script>


