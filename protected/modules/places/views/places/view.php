<div class="placeViewWrapper" ng-controller="placesAppView" ng-show="isVisible">
    <div class="closeButton" ng-click="hideView()">
        <i class="fa fa-times"></i>
    </div>
    <div class="pTitle">
        <h3>{{place.title}}</h3>
    </div>
    <div class="pDesc">
        <img src="{{place.image}}" class="descImage">
    </div>

    <div class="selectType">
        <div ng-show="countTypes">
            <switcher></switcher>
        </div>
    </div>
    <div class="pTypeName">
        <div>{{selectedType.name}}</div>
    </div>

    <div class="pPrice">
        <div>Цена за размещение в месяц</div>
        <span class="priceClass">{{selectedType.price}}</span>
    </div>

    <button ng-click="addToCart()" class="cartAdd">
        Заказать размещение
    </button>
</div>