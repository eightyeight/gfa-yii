<div class="placeOrderWrapper">
    <div class="orderInfoWrapper">
        <hr/>

        <div class="order-info">
            <ul>
                <li ng-repeat="position in cart.items">
                    <span>{{position.type}}</span>
                    <span>{{position.description}}</span>
                    <span>{{position.price}}</span>
                </li>
            </ul>
            <div class="total-price">
                Общая сумма: {{totalPrice}} руб.
            </div>
        </div>
        <hr/>
        <div class="customer-title">Ваши контактные данные</div>
        <form class="customer-info" ng-model="customer" name="customer" ng-submit="makeOrder(customer.$valid)" novalidate>
            <input type="text" name="name" ng-model="name" ng-minlength="3" ng-pattern="/^([а-я][А-Я])/i" placeholder="Ваше имя" required>
            <div class="errors">
                <p ng-show="customer.name.$error.minlength">Поле "Ваше имя" должно содержать не менее 3 символов .</p>
                <p ng-show="customer.name.$invalid && !customer.name.$pristine">Имя должно быть настоящим.</p>
            </div>



            <input type="text" name="email" ng-model="email" ng-pattern="/^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i" placeholder="Ваш e-mail" required>
            <div class="errors">
                <p ng-show="customer.email.$invalid && !customer.email.$pristine">Поле "Email" олжно быть настоящим адрессом почты.</p>
            </div>

            <input type="text" name="phone" ng-model="phone" ng-pattern="/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/" placeholder="Ваш телефон" required>
            <div class="errors">
                <p ng-show="customer.name.$invalid && !customer.name.$pristine">Поле "Телефон" должно быть телефоном.</p>
            </div>
            <button type="submit" class="make-order" ng-disabled="customer.$invalid">Оформить заказ</button>
        </form>
    </div>
</div>