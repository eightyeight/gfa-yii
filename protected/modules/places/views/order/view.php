<table>
    <tr class="table-head">
        <th>Тип рекламного места</th>
        <th>Описание</th>
        <th>Цена</th>
        <th></th>
    </tr>
    <tr ng-repeat="position in cart.items" class="table-body">
        <td>{{position.type}}</td>
        <td>{{position.description}}</td>
        <td>{{position.price}}</td>
        <td ng-click="deleteItem(position)"> Delete </td>
    </tr>
    <tr>
        <td></td>
        <td>Итого</td>
        <td>{{cart.getTotalPrice()}}</td>
    </tr>
</table>

<button class="make-order" ng-click="makeOrder()">
    <span>Оформить заказ</span>
</button>

<button class="cancel" ng-click="cancelOrder()">
    <span>Вернуться обратно</span>
</button>