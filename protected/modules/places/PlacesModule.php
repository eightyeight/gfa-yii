<?php

/**
 * Created by PhpStorm.
 * User: web
 * Date: 15.12.14
 * Time: 16:32
 */
class PlacesModule extends CWebModule
{

    public $defaultController = "places";

    public $layout = 'application.views.layouts.site';

    /**
     * @param $str
     * @param $params
     * @param $dic
     * @return string
     */
    public static function t($str = '', $params = array(), $dic = 'news')
    {
        if (Yii::t("PlacesModule", $str) == $str)
            return Yii::t("PlacesModule." . $dic, $str, $params);
        else
            return Yii::t("PlacesModule", $str, $params);
    }

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
            'places.models.*',
            'places.components.*',
            'places.controllers.*',
            'places.widgets.*'
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

} 