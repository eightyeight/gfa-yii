<?php

/**
 * Created by PhpStorm.
 * User: web
 * Date: 23.12.14
 * Time: 16:19
 */
class PlaceOrderForm extends CFormModel
{

    public $placename;

    public $placeType;

    public $price;

    public $phone;

    public $name;

    public $email;

    public function rules()
    {
        return array(

            array('phone, name', 'required'),
            array('email', 'email'),
            array('phone, name, email, price, placename, placeType', 'safe')

        );
    }

    public function attributeLabels()
    {
        return array(
            'phone' => 'Ваш телефон',
            'name' => 'Ваше имя',
            'email' => 'Ваш email'
        );
    }
} 