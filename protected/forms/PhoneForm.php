<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 01.08.14
 * Time: 14:07
 *
 * Phone Form class.
 */

class PhoneForm extends CFormModel
{
    public $phone;

    public $name;

    public $email;

    public function rules()
    {
        return array(

            array('phone, name', 'required'),
            array('email','email'),
            array('phone, name, email', 'safe')

        );
    }

    public function attributeLabels()
    {
        return array(
            'phone'=>'Ваш телефон',
            'name' =>'Ваше имя',
            'email'=>'Ваш email'
        );
    }


} 