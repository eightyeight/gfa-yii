<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 15.08.14
 * Time: 11:22
 */

class PromoForm extends CFormModel
{

    public $phone;

    public $name;

    public $email;

    public function rules()
    {
        return array(

            array('phone, name','required'),
            array('email','email'),
            array('phone, name, email', 'safe'),

        );
    }

    public function attributeLabels()
    {
        return array(
            'name'=>'Введите Имя',
            'phone'=>'Введите телефон'
        );
    }

} 