<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 10.12.14
 * Time: 18:01
 */

class ServiceAPI extends CComponent {

    public $curl;

    public static $apiURI;

    /**
     * @var bool
     */
    public $ssl_verifypeer = false;

    /**
     * @var bool
     */
    public $returntransfer = true;

    public function __construct(){
        $this->init();
    }


    public function init(){
        if(!$this->curl){
            $this->curl = new \Curl\Curl();
            $this->curl->setopt(CURLOPT_RETURNTRANSFER, TRUE);
            $this->curl->setopt(CURLOPT_SSL_VERIFYPEER, FALSE);
        }
    }

    /**
     * @return Curl object
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @return string
     */
    public static function getApiURI()
    {
        return static::$apiURI;
    }
} 