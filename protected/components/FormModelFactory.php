<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 07.11.14
 * Time: 12:44
 */

class FormModelFactory {

    private static $instance;

    private $request;

    private $modelClassName;

    /**
     * @param mixed $instance
     *w
     */
    private static function setInstance()
    {
        if(!self::$instance)
            self::$instance = new FormModelFactory;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if(!self::$instance)
            self::setInstance();
        return self::$instance;
    }

    /**
     * @param mixed $request
     */
    public function setRequest(CHttpRequest $request)
    {
        $this->request = $request;
    }

    /**
     * @param mixed $modelClassName
     */
    public function setModelClassName($modelClassName)
    {
        $this->modelClassName = $modelClassName;
    }

    /**
     * @return mixed
     */
    public function getModelClassName()
    {
        return $this->modelClassName;
    }

    public function parseHttpRequest($request)
    {
        $this->modelClassName = $request->getPost('className');
    }

    public function createModel()
    {
        if(isset($this->request) && $this->request instanceof CHttpRequest){

            try{
                $this->parseHttpRequest($this->request);

                $_model = new $this->modelClassName;

                $_model->attributes = $this->request->getPost($this->modelClassName);

                return $_model;
            }
            catch(Exeption $e){

                echo $e->getMessage();

            }
        }
        else{
            throw new Exception('Request object not set or not instance of CHttpRequest class. Use setRequest method before create models, also check param was passed is object of CHttpRequest class');
        }
    }
}