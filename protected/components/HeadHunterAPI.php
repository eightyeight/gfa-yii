<?php
/**
 * hh.ru API integration class;
 */

class HeadHunterAPI extends ServiceAPI {

    /**
     * @var string
     */
    public static $apiURI = 'https://api.hh.ru/';

    /**
     * @var int
     */
    public $companyID = 1157389;

    /**
     * @var string
     */
    public $redirectURI = 'http://gf-a.ru';

    /**
     * @var string
     */
    public $clientID = 'NV7NNAR812ID7BDSOT39E0K57N28PQCBLAC0KTOQL635NLH59AG3EMHM30M20DU8';

    /**
     * @var string
     */
    public $clientSecret = 'O8I19EB3E48Q89K7LG6N5PD5NPFAU7M3HS7GER1905IMSJI3AQT790BJ24DL91S4';


    /**
     * @param string $clientID
     */
    public function setClientID($clientID)
    {
        $this->clientID = $clientID;
    }

    /**
     * @return string
     */
    public function getClientID()
    {
        return $this->clientID;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param int $companyID
     */
    public function setCompanyID($companyID)
    {
        $this->companyID = $companyID;
    }

    /**
     * @return int
     */
    public function getCompanyID()
    {
        return $this->companyID;
    }

    /**
     * @param string $redirectURI
     */
    public function setRedirectURI($redirectURI)
    {
        $this->redirectURI = $redirectURI;
    }

    /**
     * @return string
     */
    public function getRedirectURI()
    {
        return $this->redirectURI;
    }

    /**
     * @param boolean $returntransfer
     */
    public function setReturntransfer($returntransfer)
    {
        $this->returntransfer = $returntransfer;
    }

    /**
     * @return boolean
     */
    public function getReturntransfer()
    {
        return $this->returntransfer;
    }

    /**
     * @param boolean $ssl_verifypeer
     */
    public function setSslVerifypeer($ssl_verifypeer)
    {
        $this->ssl_verifypeer = $ssl_verifypeer;
    }

    /**
     * @return boolean
     */
    public function getSslVerifypeer()
    {
        return $this->ssl_verifypeer;
    }

    /**
     * @return array of all Vacancies of company
     * @throws Exception cURL error message
     */
    public function getVacancies(){

        $curl = $this->curl;

        $curl->get(self::$apiURI . 'vacancies',array(
            'employer_id' => $this->companyID,
        ));

        if ($curl->error)
            throw new Exception($curl->error_message);
        else
            return CJSON::decode($curl->response);

    }

    public function getVacancy($id){

        $curl = $this->curl;

        $curl->get(self::$apiURI . 'vacancies/'.$id);

        if ($curl->error)
            throw new Exception($curl->error_message);
        else
            return CJSON::decode($curl->response);
    }


    public function getVacanciesData(){

        $result = array();

        $vacancies = $this->getVacancies();

        foreach($vacancies['items'] as $vacancy):
            array_push($result, $this->getVacancy($vacancy['id']));
        endforeach;

        return $result;
    }

} 