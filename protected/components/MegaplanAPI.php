<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 10.12.14
 * Time: 17:53
 */

class MegaplanAPI  extends ServiceAPI{

    public static $apiURI = 'https://gfa.megaplan.ru';

    public $login = 'bodrov@gf-a.ru';

    public $password = 'LBKfBWN93d8R';

    //string ID доступа для формирования сигнатуры запросов
    private $accessId = 'bb7Eb644a209adcce791';

    //string Секретный ключ для формирования сигнатуры запросов
    private $secretKey = '847558505b8a4eDa8D59D0Faa8ae9708a246b730';

    //integer Идентификатор залогиневшегося пользователя
    private $userId = 1000132;

    //integer Идентификатор залогиневшегося сотрудника
    private $employeeId = 1000087;

    public function init(){
        parent::init();

        if(isset($this->password))
            $this->password = md5($this->password);


        $this->curl->setopt(CURLOPT_FOLLOWLOCATION, TRUE);
    }

    public function authenticate(){
        $this->curl->setHeader('Accept', 'application/json');
        $this->curl->get('http://gfa.megaplan.ru/BumsCommonApiV01/User/authorize.api', array(
            'Login' => $this->login,
            'Password' => $this->password,
        ));

        if($this->curl->error)
            echo $this->curl->error;
        else
            echo $this->curl->response;

        var_dump($this->curl->request_headers);
        var_dump($this->curl->response_headers);


    }

    public function getEmployeeList(){

        $requestUri = '/BumsStaffApiV01/Employee/list.xml';
        $date = $this->getDate();
        $contentType = 'application/json';
        $signature = $this->createSignature('GET',$requestUri,$this->secretKey,$date);


        $this->curl->setHeader('Date',$date );
        $this->curl->setHeader('Accept',$contentType);
//        $this->curl->setHeader('Content-Type',$contentType);
        $this->curl->setHeader('X-Authorization', $this->accessId . ':' . $signature);
        $this->curl->get('https://gfa.megaplan.ru/BumsStaffApiV01/Employee/list.xml', array(
            'Department' => -1,
        ));

        echo $this->curl->response;

        var_dump($this->curl->request_headers);
        var_dump($this->curl->response_headers);
    }

    public function createSignature($method, $requestUri, $secretKey,  $date)
    {

        $stringToSign =  $method . "\n".
            "\n" .
            "\n" .
            $date . "\n" .
            "fa.megaplan.ru" . $requestUri;

        $signature = base64_encode(hash_hmac( 'sha1', $stringToSign, $secretKey ));

        return $signature;
    }

    public function getDate(){
        $date = new DateTime('NOW');
        return $date->format(DateTime::RFC2822);
    }

    public static function hashHmac( $Algo, $Data, $Key, $RawOutput = false )
    {
        if ( function_exists( 'hash_hmac' ) ) {
            return hash_hmac( $Algo, $Data, $Key, $RawOutput );
        }
        $Algo = strtolower( $Algo );
        $pack = 'H' . strlen( $Algo( 'test' ) );
        $size = 64;
        $opad = str_repeat( chr( 0x5C ), $size );
        $ipad = str_repeat( chr( 0x36 ), $size );

        if ( strlen( $Key ) > $size ){
            $Key = str_pad( pack( $pack, $Algo( $Key ) ), $size, chr( 0x00 ) );
        } else {
            $Key = str_pad( $Key, $size, chr( 0x00 ) );
        }

        for ( $i = 0; $i < strlen( $Key ) - 1; $i++ ) {
            $opad[$i] = $opad[$i] ^ $Key[$i];
            $ipad[$i] = $ipad[$i] ^ $Key[$i];
        }

        $output = $Algo( $opad.pack( $pack, $Algo( $ipad.$Data ) ) );

        return ( $RawOutput ) ? pack( $pack, $output ) : $output;
    }

} 