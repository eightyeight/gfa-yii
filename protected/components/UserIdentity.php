<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends EAuthUserIdentity
{

    /**
     * Overwrite authenticate method.
     * Saving new user to database.
     * @return bool
     */
    public function authenticate()
	{

        $attributes = $this->service->attributes;

        $user = User::model()->with('profile')->findByAttributes(array(
            'service' => $this->service->getServiceName(),
            'service_id' => $attributes['service_id']
        ));
        if (!$user) {

            $user    = new User;
            $profile = new Profile;

            $user->username   = $attributes['username'];
            $user->superuser  = 0;
            $user->status     = 1;
            $user->service    = $this->service->getServiceName();
            $user->service_id = $attributes['service_id'];
            $user->email      = $attributes['email'];

            if($user->save()){
                $profile->user_id = $user->getAttribute('id');
                $profile->lastname = $attributes['last_name'];
                $profile->firstname = $attributes['first_name'];
                $profile->userpic = $attributes['picture'];
                $profile->save();

                    $authorizer = Yii::app()->getModule("rights")->getAuthorizer();
                    $authorizer->authManager->assign('Authenticated', $user->getAttribute('id'));
            }

        }
        else{
            $user->profile->userpic = $attributes['picture'];
            $user->update();
        }

        $this->setState('__id',$user->getAttribute('id'));


        return parent::authenticate();
	}

}