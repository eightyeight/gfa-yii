<?php
/* @var $this PortfolioController */

// Register Js Scripts...
Yii::app()->clientScript
    ->registerScriptFile('/js/vendor/'.'modernizr-2.6.2-respond-1.1.0.min.js',CClientScript::POS_HEAD)
    ->registerScriptFile('/js/vendor/'.'imagesloaded.pkgd.min.js',CClientScript::POS_HEAD)
    ->registerScriptFile('/js/vendor/'.'isotope.pkgd.js',CClientScript::POS_HEAD)
    ->registerScript('isotopeInit'," document.addEventListener('DOMContentLoaded', function () {
                                var container = document.querySelector('.grid');
                                var iso;

                                var imgLoad = imagesLoaded( container, function() {

                                     iso = new Isotope( container, {
                                      // options...
                                      itemSelector: '.item',
                                      masonry: {
                                        columnWidth: 10,
                                        isFitWidth: true
                                      }

                                     });

                                });

                                imgLoad.on( 'progress', function( instance, image ) {
                                  $('.loader-foo').show();
                                });

                                imgLoad.on( 'done', function() {
                                  $('.loader-foo').hide();

                                });

                                new grid3D( document.getElementById( 'grid3d' ) );

                                $('.load').delay('2000').fadeOut('slow')


                            });", CClientScript::POS_HEAD)
;
?>


<div  >
    <div class="clearfix">

    </div>

    <section class="grid3d vertical alt" id="grid3d" >
        <div class="grid-wrap">
            <div class="grid">
                <figure class="item effect-lily">
                    <img src="images/portfolio/berry-01.png" alt="images01"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/darvin-05.png" alt="images05"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/lindegroup-01.png" alt="images08"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/metr-02.png" alt="images02"/>
                    <figcaption>
                        <h2> <span></span> Метр</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/orchid-01.png" alt="images04"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/vac-01.png" alt="images03"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/optika-01.png" alt="images09"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/buhov-01.png" alt="images06"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/danish-01.png" alt="images07"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/little-tokio-01.png" alt="images07"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/panorama-01.png" alt="images07"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/vintazh-01.png" alt="images07"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
                <figure class="item effect-lily">
                    <img src="images/portfolio/anastasia-01.png" alt="images07"/>
                    <figcaption>
                        <h2> <span>кафе</span> Berry</h2>
                        <div></div>
                        <p>Milo went to the woods. He took a fun ride and never came back.</p>
                    </figcaption>
                </figure>
            </div>
        </div><!-- /grid-wrap -->
        <div class="content">
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <div>
                <div class="dummy-images"></div>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
                <p class="dummy-text">The only people for me are the mad ones, the ones who are mad to live, mad to talk, mad to be saved, desirous of everything at the same time, the ones who never yawn or say a commonplace thing, but burn, burn, burn like fabulous yellow roman candles exploding like spiders across the stars.</p>
            </div>
            <span class="loading">
                <ul class="loader">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </span>
            <span class="icon close-content"></span>
        </div>
    </section>

            <div class="load">
                <ul class="loader">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>

</div>

