<div class="">
    <h2 class="section-header">Наша команда</h2>
    <div class="row">

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/IMG_5395-3.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Жуковец Станислав</p>
            <p class="column-content">Генеральный директор</p>

            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/06e1e76265d448c29af0b919491c30bb500_375.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Дягель Надежда</p>
            <p class="column-content">Руководитель Отдела продаж</p>

            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/IMG_9625-5.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Бирюлина Надежда</p>
            <p class="column-content">Главный бухгалтер</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/IMG_5474-3.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Хохлов Денис</p>
            <p class="column-content">Главный дизайнер</p>

            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>
    </div>
    <div class="row">



        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/42688cd2df49991b380d27b07c36b3ae500_375.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Малютина Вилена</p>
            <p class="column-content">Секретарь</p>

            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/477874_113443105451620_1821890963_o.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Абдрахманов Дмитрий</p>
            <p class="column-content">Весельчак</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/bE1_p76ezmw.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Сафронов Андрей</p>
            <p class="column-content">Дизайнер</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/607b53b37e9c5536257d078a3cafa929500_375.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Бодров Илья</p>
            <p class="column-content">Web-разработчик</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>
    </div>

    <div class="row">

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/bfb4e98a3b0abbb2a9fbe1d3c4bc1a28500_375.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Никитина Анжелика</p>
            <p class="column-content">Менеджер по продажам</p>

            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/13ce33af98fbcf1878170be2061141f3500_375.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Темирбеков Руслан</p>
            <p class="column-content">Менеджер по продажам</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <img src="/images/team/e33aca6695a865f50b0259a101026932500_375.jpg" alt="">
                </div>
            </div>
            <p class="column-header">Закатин Максим</p>
            <p class="column-content">Руководитель монтажных бригад</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>

        </div>

        <div class="column 3u">
            <div class="img-team">
                <div class="imgInner">
                    <div class="imgInner">
                        <img src="/images/team/f62746807bd4f1689f2b96eeaa1f641d500_375.jpg" alt="">
                    </div>
                </div>
            </div>
            <p class="column-header">Шевцова Олеся</p>
            <p class="column-content">Технолог производства</p>
            <div class="social-links">
                <a href="#"><span class="tw-icon"></span></a>
                <a href="#"><span class="fb-icon"></span></a>
                <a href="#"><span class="pin-icon"></span></a>
            </div>
        </div>
    </div>
</div>