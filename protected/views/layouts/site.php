<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Рекламное Агенство Жираф</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="лайтбоксы,вывески,наружная реклама,световые короба,изготовление вывесок,неоновые вывески,фасадные вывески,сделать сайт,печать баннеров,вывеска магазина,заказать баннер,наружные рекламы,световой короб,световая реклама,объемные буквы,штендер,lightbox, дизайн, объемные буквы купить,объемные буквы изготовление,объемные буквы заказ,объемные буквы изготовить,объемные буквы ,объемные буквы производство,рекламное агентство,реклама солнечногорск,жираф, оклейка авто, брендирование авто" />
    <meta name='yandex-verification' content='6fa174ed733228bd' />
    <meta name="google-site-verification" content="znKLH4AnbQk38Vn3VvW7C1imi4T1Z7qxclFTaAjK-_4" />
    <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
    <?php Yii::app()->clientScript->registerScriptFile('/js/callform.js')?>


    <script src="js/jquery.bxslider.js"></script>
    <script src="js/vendor/jquery.inputmask.js"></script>
    <script src="js/main.js"></script>
    <link rel="stylesheet" href="css/jquery.bxslider.css">



    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/vendor/animate.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/skel.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/style.css" />


    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->


    <style type="text/css">
        #map {
            width: 100%;
            height: 400px;
        }
        a[href^="http://maps.google.com/maps"]{display:none !important}

        .gmnoprint a, .gmnoprint span {
            display:none;
        }
        .gmnoprint div {
            background:none !important;
        }
    </style>



    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>

    <script type="text/javascript">

        google.maps.event.addDomListener(window, 'load', init);

        function init() {

            var mapOptions = {
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false,
                zoom: 15,
                disableDefaultUI: true,
                center: new google.maps.LatLng(56.179924, 37.003441),
                styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]}]
            };

            var mapElement = document.getElementById('map');

            var map = new google.maps.Map(mapElement, mapOptions);

            marker = new google.maps.Marker({
                map:map,
                // draggable:true,
                // animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng(56.17970, 37.00359),
                icon: 'http://gf-a.ru/marker.png' // null = default icon
            });
        }

    </script>
</head>
<body class="index">
<?php $call_form_model = new PhoneForm();?>
<div class="popup">
    <?php $cform  = $this->beginWidget('CActiveForm',array(
        'id' => 'call-form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'action'=> CHtml::normalizeUrl(array('sendmail/send')),
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>false,
        ),
    ))?>

    <input type="hidden" name="className" value="<?php echo get_class($call_form_model)?>">
    <?php echo $cform->textField($call_form_model,'name', array('placeholder'=>'Введите Имя', 'class'=>'promo-input', 'id' => 'name'));?>
    <?php echo $cform->error($call_form_model,'name', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));?>
    <?php echo $cform->textField($call_form_model,'phone', array('placeholder'=>'Введите Телефон', 'class'=>'promo-input', 'id' => 'phone'));?>
    <?php echo $cform->error($call_form_model,'phone', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));?>
    <?php echo $cform->textField($call_form_model,'email', array('placeholder'=>'Введите Телефон', 'class'=>'promo-input', 'id' => 'phone'));?>
    <?php echo $cform->error($call_form_model,'email', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));?>
    <?php echo CHtml::ajaxSubmitButton('Написать', $this->createUrl('sendmail/send'),
        array(
            'type'=>'POST',
            'success'=>'function(data){
                                                console.log(data);
                                                var data = jQuery.parseJSON(data);

                                                if(data.status=="success"){

                                                    console.log("success")

                                                }
                                                else{
                                                        jQuery.each(data, function(key, value) {
                                                            jQuery("#"+key+"_em_").show().html(value.toString());
                                                        });
                                                }
                                            }'
        ),
        array(
            'class' => 'button special',
        )
    );?>
    <?php $this->endWidget()?>
</div>

<!-- Content -->
    <?php echo $content; ?>
<!-- End Content -->

<!-- Footer -->
    <?php require_once ('footer.php'); ?>
<!-- End Footer -->


</body>
</html>
