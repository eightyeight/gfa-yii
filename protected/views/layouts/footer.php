<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 17.09.14
 * Time: 12:10
 */
?>
<footer id="footer" class="alt">
    <div class="row">
        <div class="2u">
            <?php $this->widget('zii.widgets.CMenu',array(
    'items'=>array(
        array('label'=>'Портфолио', 'url'=>array('site/portfolio')),
        array('label'=>'Услуги', 'url'=>array('site/services')),
        array('label'=>'Кто мы', 'url'=>array('/site/about')),
        array('label'=>'Вакансии', 'url'=>array('/site/vacancy')),
        array('label'=>'Контакты', 'url'=>array('/site/contact')),
    ),
    'htmlOptions' => array('class' => 'bottom-menu')
)); ?>

</div>
<div class="8u" style="text-align: left;
        font-size: 15px;
    line-height: 28px;
    letter-spacing: 1px;
        ">
    <p>ООО "Перфектив"<br>
        Юр.адрес: 141506, Московская область, г. Солнечногорск, ул. Красная, д. 120<br>
        ИНН 5044074288<br>
        ОГРН 1105044001677</p>

</div>
<div class="2u">
    <div class="social-block">
        <div class="phone">8(495) 215-19-22</div>
        <ul class="social-icons ">
            <li> <a href="https://www.facebook.com/groups/zhiraf/"><i class="fa fa-facebook"></i></a></li>
            <li> <a href="https://vk.com/ra_giraffe"><i class="fa fa-vk"></i></a></li>
        </ul>
    </div>
    <span class="copyright">&copy; РА Жираф 2014</span>
</div>
</div>
</footer>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter26174976 = new Ya.Metrika({id:26174976,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/26174976" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-54798773-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- /Google Analytics -->