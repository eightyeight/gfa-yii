<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 18.09.14
 * Time: 16:39
 */
?>

<!DOCTYPE HTML>

<html>
<head>
    <title>Рекламное Агенство Жираф</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/jquery.dropotron.min.js"></script>
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/vendor/jquery.inputmask.js"></script>
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/skel.min.js"></script>
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/skel-layers.min.js"></script>
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/init.js"></script>
    <script src="<?php echo Yii::app()->request->hostInfo; ?>/js/main.js"></script>


    <link rel="stylesheet" href="<?php echo Yii::app()->request->hostInfo; ?>/css/skel.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->hostInfo; ?>/css/style.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->hostInfo; ?>/css/style-wide.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->hostInfo; ?>/css/style-noscript.css" />

    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->

</head>
<body class="index loading">

<!-- Content -->
<?php echo $content; ?>
<!-- End Content -->
<!-- Footer -->
<?php require_once ('footer.php'); ?>
<!-- End Footer -->
</body>
</html>
