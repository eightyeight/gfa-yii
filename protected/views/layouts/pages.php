<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Рекламное Агенство Жираф</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="лайтбоксы,вывески,наружная реклама,световые короба,изготовление вывесок,неоновые вывески,фасадные вывески,сделать сайт,печать баннеров,вывеска магазина,заказать баннер,наружные рекламы,световой короб,световая реклама,объемные буквы,штендер,lightbox, дизайн, объемные буквы купить,объемные буквы изготовление,объемные буквы заказ,объемные буквы изготовить,объемные буквы ,объемные буквы производство,рекламное агентство,реклама солнечногорск,жираф, оклейка авто, брендирование авто" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
    <?php Yii::app()->clientScript->registerScriptFile('/js/callform.js')?>

    <script src="/js/vendor/jquery.inputmask.js"></script>

    <script src="/js/menu.js"></script>

    <link rel="stylesheet" href="/css/skel.css" />
    <link rel="stylesheet" href="/css/style.css" />
    <link rel="stylesheet" href="/css/vendor/animate.css" />




    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->


<!--    <style type="text/css">-->
<!--        #map {-->
<!--            width: 100%;-->
<!--            height: 400px;-->
<!--        }-->
<!--        a[href^="http://maps.google.com/maps"]{display:none !important}-->
<!---->
<!--        .gmnoprint a, .gmnoprint span {-->
<!--            display:none;-->
<!--        }-->
<!--        .gmnoprint div {-->
<!--            background:none !important;-->
<!--        }-->
<!--    </style>-->



<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>-->



    <script>
        $(document).ready(function(){

            $('#phone').inputmask("mask", {"mask": "+7 (999) 999-9999"});

            $('.menuBtn').on('click', function(){

                $('#secondary-menu').toggle(600);

                if($('.menuBtn').hasClass('closeBtn')){
                    $('#pointer').animate({top:'42px'},600);
                    $('.menuBtn').removeClass('closeBtn');
                }
                else{
                    $('#pointer').animate({top:'92px'},600);
                    $('.menuBtn').addClass('closeBtn');
                }

            });

        });
    </script>



</head>
<body class="index">

<!-- Header -->

<header id="header" class="page">
    <?php $call_form_model = new PhoneForm();?>
    <div class="popup">
        <?php $cform  = $this->beginWidget('CActiveForm',array(
            'id' => 'call-form',
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
            'action'=> CHtml::normalizeUrl(array('sendmail/send')),
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>false,
            ),
        ))?>

        <input type="hidden" name="className" value="<?php echo get_class($call_form_model)?>">
        <?php echo $cform->textField($call_form_model,'name', array('placeholder'=>'Введите Имя', 'class'=>'promo-input', 'id' => 'name'));?>
        <?php echo $cform->error($call_form_model,'name', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));?>
        <?php echo $cform->textField($call_form_model,'phone', array('placeholder'=>'Введите Телефон', 'class'=>'promo-input', 'id' => 'phone'));?>
        <?php echo $cform->error($call_form_model,'phone', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));?>
        <?php echo $cform->textField($call_form_model,'email', array('placeholder'=>'Введите Телефон', 'class'=>'promo-input', 'id' => 'phone'));?>
        <?php echo $cform->error($call_form_model,'email', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));?>
        <?php echo CHtml::ajaxSubmitButton('Написать', $this->createUrl('sendmail/send'),
            array(
                'type'=>'POST',
                'success'=>'function(data){
                                                console.log(data);
                                                var data = jQuery.parseJSON(data);

                                                if(data.status=="success"){

                                                    alert("Success");

                                                }
                                                else{
                                                        jQuery.each(data, function(key, value) {
                                                            jQuery("#"+key+"_em_").show().html(value.toString());
                                                        });
                                                }
                                            }'
            ),
            array(
                'class' => 'button special',
            )
        );?>
        <?php $this->endWidget()?>
    </div>

    <nav id="nav" class="fixed pages inverse">

        <div id="secondary-menu" class="secondary-menu">

            <div id="call-button">
<!--                <img src="/images/phone-close-circle-512.png" class="phone-icon" style="height: 40px">-->
                <img src="/images/mobile.png">
                <span>Заказать звонок</span>
            </div>
            <div>
                <a href="/construction">
                    <object id="constructions" data="<?Yii::app()->request->hostInfo?>/images/icons/construct.svg" type="image/svg+xml" style="height: 25px;"></object>
                    <span>Рекламные места</span>
                </a>
            </div>
            <div >
                <a href="/payment/index">
                    <object id="payment" data="<?Yii::app()->request->hostInfo?>/images/icons/wallet.svg" type="image/svg+xml" style="height: 25px;"></object>
                    <span>оплата</span>
                </a>
            </div>
            <div>
                <?php if(Yii::app()->user->isGuest):?>
                    <?php
                    $this->widget('application.widgets.loginModalFormWidget.LoginFormModal');
                    ?>
                <?php else:?>
                    <?php
                    $this->widget('application.widgets.userInfoWidget.UserInfoWidget');
                    ?>
                <?php endif;?>
            </div>

        </div>

        <?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('template'=>'<a href="/"><img src="/images/logo-white.png" height="50px"></a>', 'url'=>array('site/index'), 'itemOptions' => array('class' => 'navItem')),
                array('label'=>'Портфолио', 'url'=>array('/site/portfolio'), 'itemOptions' => array('class' => 'navItem')),
                array('label'=>'Услуги', 'url'=>array('/site/services'), 'itemOptions' => array('class' => 'navItem')),
                array('label'=>'Кто мы', 'url'=>array('/about/index'), 'itemOptions' => array('class' => 'navItem'),
                    'submenuOptions'=>array('class'=>'nav-sub'),
                    'items'=>array(
                        array('label'=>'Новости', 'url'=>array('/about/news')),
                        array('label'=>'Команда', 'url'=>array('/about/team')),
                        array('label'=>'Отзывы', 'url'=>array('/about/review')),
                    ),
                ),
                array('label'=>'Вакансии', 'url'=>array('/site/vacancy'), 'itemOptions' => array('class' => 'navItem')),
                array('label'=>'Контакты', 'url'=>array('/site/contact'), 'itemOptions' => array('class' => 'navItem')),
                array('template'=>'<a href="#" class="menuBtn"><span></span></a>','itemOptions' => array('style' => 'float: right;position: absolute;right: 0px;')),
                array('itemOptions' => array('id' => 'pointer', 'class' => 'pointer')),
            ),
            'htmlOptions'=>array(
                'class' => 'navInner'
            )
        )); ?>
    </nav>

</header>
<!-- Content -->
<?php echo $content; ?>
<!-- Footer -->
<?php require_once ('footer.php'); ?>
<!-- End Footer -->
<script>
    $(window).load(function(){
        var menuAnimation = new menuFixedHoverEffect('.navInner');

        menuAnimation.run();
    })
</script>
</body>
</html>
