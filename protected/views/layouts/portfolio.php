<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Рекламное Агенство Жираф</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>

    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/jquery.bxslider.js"></script>

    <link rel="stylesheet" href="css/jquery.bxslider.css">


    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/style.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/style-wide.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/style-noscript.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/portfolio/component.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/3dgrid/component.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>css/portfolio/normalize.css" />

    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->




</head>
<body class="">

<!-- Header -->

<header id="header" class="pages">

    <div id="logo" class='logo'>
        <a href="<?php echo Yii::app()->createUrl('site/index');?>"><img src="images/logo-white.png" height="50px" class='alt'></a>
    </div>
    <nav id="nav">
        <?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>'Портфолио', 'url'=>array('/portfolio/index')),
                array('label'=>'О Нас', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Вакансии', 'url'=>array('/site/page', 'view'=>'vacancy')),
                array('label'=>'Команда', 'url'=>array('/site/page', 'view'=>'team')),
                array('label'=>'Контакты', 'url'=>array('/site/contact')),
            ),
        )); ?>
    </nav>

</header>
<!-- Content -->
<?php echo $content; ?>
<!-- Footer -->
<footer id="footer" class="alt">
    <div class="row">
        <div class="4u">
            <?php $this->widget('zii.widgets.CMenu',array(
                'items'=>array(
                    array('label'=>'Портфолио', 'url'=>array('/portfolio/index')),
                    array('label'=>'О Нас', 'url'=>array('/site/page', 'view'=>'about')),
                    array('label'=>'Вакансии', 'url'=>array('/site/page', 'view'=>'vacancy')),
                    array('label'=>'Команда', 'url'=>array('/site/page', 'view'=>'team')),
                    array('label'=>'Контакты', 'url'=>array('/site/contact')),
                ),
                'htmlOptions' => array('class' => 'bottom-menu')
            )); ?>

        </div>
        <div class="4u">
            <span class="copyright">&copy; РА Жираф 2014</span>
        </div>
        <div class="4u">

            <div class="social-block">
                <div class="phone">8(926) 215-19-22</div>
                <ul class="social-icons ">
                    <li> <a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li> <a href="#"><i class="fa fa-vk"></i></a></li>
                    <li> <a href="#"><i class="fa fa-phone"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


</body>
</html>
