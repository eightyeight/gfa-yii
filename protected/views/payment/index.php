<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 29.08.14
 * Time: 10:48
 */



?>

<header class="payment">
    <img src="<?Yii::app()->request->hostInfo?>/images/logo.png" style="margin:0 auto;">

    <div>

            <h1>Единая Система Оплаты Услуг Онлайн  
                <a href="#" title="Оплата услуг производится через систему Робокасса.">
                    <sup>
                        <i class="fa fa-question"></i>
                    </sup>
                </a>
            </h1>

    </div>

</header>

<div class="container" style="font-family: 'Open Sans'">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'pay-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

        <div class="-2u 8u" style="padding: 10px;">
            <?php echo $form->dropDownList($invoice, 'typeId', CHtml::listData(ServiceType::model()->findAll(), 'id', 'name'), array('empty'=>'Выберите услугу')); ?>
        </div>

        <div class="-2u 8u" style="padding: 10px;">
            <?php echo $form->textField($invoice,'email', array('placeholder'=>'Почта плательщика')); ?>
            <?php echo $form->error($invoice,'email'); ?>
        </div>

        <div class="-2u 8u" style="padding: 10px;">
            <?php echo $form->textField($invoice,'amount', array('placeholder'=>'Сумма платежа')); ?>
            <?php echo $form->error($invoice,'amount'); ?>
        </div>

        <div class="-2u 8u" style="padding: 10px;">
            <?php echo $form->textArea($invoice,'description', array('placeholder'=>'Описание платежа')); ?>
            <?php echo $form->error($invoice,'description'); ?>
        </div>

        <div class="-2u 8u" style="padding: 10px;">
            <ul class="buttons">
                <li class="contact">
                    <button type="submit" class="button special">
                        Оплатить
                    </button>
                </li>
            </ul>
        </div>
    <?php $this->endwidget();?>

</div>
