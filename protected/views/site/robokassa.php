<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 28.08.14
 * Time: 12:27
 */
Yii::import('ext.yii-robokassa.components.Robokassa');
echo CHtml::openTag('div');

$this->widget('\Robokassa\Widgets\PaymentForm', array(
    'baseUrl' => Robokassa::URL_TEST,
    'invoice' => $invoice,
));

echo CHtml::closeTag('div');