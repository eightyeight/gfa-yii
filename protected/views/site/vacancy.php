<div style="background: rgb(224,224,224); /* Old browsers */
background: -moz-radial-gradient(center, ellipse cover,  rgba(224,224,224,1) 9%, rgba(188,188,188,1) 55%, rgba(127,127,127,1) 100%); /* FF3.6+ */
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(9%,rgba(224,224,224,1)), color-stop(55%,rgba(188,188,188,1)), color-stop(100%,rgba(127,127,127,1))); /* Chrome,Safari4+ */
background: -webkit-radial-gradient(center, ellipse cover,  rgba(224,224,224,1) 9%,rgba(188,188,188,1) 55%,rgba(127,127,127,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-radial-gradient(center, ellipse cover,  rgba(224,224,224,1) 9%,rgba(188,188,188,1) 55%,rgba(127,127,127,1) 100%); /* Opera 12+ */
background: -ms-radial-gradient(center, ellipse cover,  rgba(224,224,224,1) 9%,rgba(188,188,188,1) 55%,rgba(127,127,127,1) 100%); /* IE10+ */
background: radial-gradient(ellipse at center,  rgba(224,224,224,1) 9%,rgba(188,188,188,1) 55%,rgba(127,127,127,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e0e0e0', endColorstr='#7f7f7f',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
overflow: hidden;
">

    <div style="
    background-position: top left, top right;background-repeat: repeat-y, repeat-y; background-image: url('images/dotstrokes.png'), url('images/dotstrokes-right.png') ; background-attachment: fixed; background-size: inherit;">
        <section id="banner" style="height: 300px;
        background: transparent;">
            <canvas id="demo-canvas"></canvas>
            <div class="quote">
                <blockquote>
                    Не имеет смысла нанимать толковых людей, а затем указывать, что им
                    делать. Мы нанимаем толковых людей, чтобы они говорили, что делать нам.
                </blockquote>
                <p>Стив Джобс</p>
            </div>
        </section>
        <div class="container"  style="padding: 5em 0em;min-height: 352px;">
            <?php $this->widget('application.widgets.vacanciesWidget.VacanciesWidget'); ?>
        </div>
    </div>
</div>


