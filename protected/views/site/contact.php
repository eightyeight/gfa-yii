<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */


?>

<div style="padding-top: 5em">

    <?php if(Yii::app()->user->hasFlash('contact')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('contact'); ?>
    </div>

    <?php else: ?>




    <div class="form">
        <section >

         <header class="container">
            <div class="row">
                <div class=" 8u contact-form">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'contact-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>

                    <div class="row half no-collapse-1">
                        <div class="6u">
                            <?php echo $form->textField($model,'name', array('placeholder'=>'Ваше Имя')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </div>

                        <div class="6u">
                            <?php echo $form->textField($model,'email',array('placeholder'=>'E-Mail')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </div>
                    </div>
                    <div class="row half">
                        <div class="12u">
                            <?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128, 'placeholder'=>'Сообщение')); ?>
                            <?php echo $form->error($model,'subject'); ?>
                        </div>
                    </div>
                    <div class="row half">
                        <div class="12u">
                            <?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
                            <?php echo $form->error($model,'body'); ?>
                        </div>
                    </div>
                    <?php if(CCaptcha::checkRequirements()): ?>
                        <div class="row half">
                            <div >
                                <div style="display:inline-block"> <?php echo $form->textField($model,'verifyCode'); ?></div>
                                <div style="display:inline-block;position: relative;bottom: -17px;"> <?php $this->widget('CCaptcha', array('buttonLabel'=>'text','buttonType'=>'button', 'buttonOptions' => array('type' => 'image', 'src'=>'images/refresh-icon.png', 'style' => 'height: 40px; margin-left: 10px; margin-bottom: 5px'))); ?></div>

                            </div>
                            <?php echo $form->error($model,'verifyCode'); ?>
                        </div>
                    <?php endif; ?>

                        <div class="row">
                            <div class="12u">
                                <ul class="buttons">
                                    <li class="contact">
                                        <button type="submit" class="button special">
                                            <i class="fa fa-paper-plane-o"></i> Написать
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </div>
                <div class="4u info">
                    <ul>
                        <li><i class="fa fa-map-marker"></i> ул. Юности, д. 2</li>
                        <li><i class="fa fa-mobile"> +7 495 215-19-22</i></li>
                        <li><i class="fa fa-phone"> +7 800 555-28-66</i></li>
                        <li><i class="fa fa-envelope"> info@gf-a.ru</i></li>
                    </ul>
                </div>
            </div>
         </header>
                    <?php $this->endWidget(); ?>
         </section>
    </div><!-- form -->

    <?php endif; ?>
</div>
<div id="map"></div>