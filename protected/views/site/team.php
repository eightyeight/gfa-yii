<div class="container big" style="margin-top: 3em; padding-bottom: 3em">
    <div class="section-4">
        <div class="inner-wrapper">
            <h2 class="section-header">Наша команда</h2>
            <p class="section-info">Proin iaculis purus consequat sem cure.</p>
            <div class="row-section-4">

                <div class="column">
                    <div class="img-team">
                        <img src="http://medicine1.info/wp-content/uploads/2014/07/person-icon.png" alt="">
                    </div>
                    <p class="column-header">Иван Петров</p>
                    <p class="column-content">CEO</p>

                    <div class="social-links">
                        <a href="#"><span class="tw-icon"></span></a>
                        <a href="#"><span class="fb-icon"></span></a>
                        <a href="#"><span class="pin-icon"></span></a>
                    </div>

                </div><!-- Column -->

                <div class="column">
                    <div class="img-team">
                        <img src="http://medicine1.info/wp-content/uploads/2014/07/person-icon.png" alt="">
                    </div>
                    <p class="column-header">Иван Петров</p>
                    <p class="column-content">Граффический дизайнер</p>

                    <div class="social-links">
                        <a href="#"><span class="tw-icon"></span></a>
                        <a href="#"><span class="fb-icon"></span></a>
                        <a href="#"><span class="pin-icon"></span></a>
                    </div>

                </div><!-- Column -->

                <div class="column">
                    <div class="img-team">
                        <img src="http://medicine1.info/wp-content/uploads/2014/07/person-icon.png" alt="">
                    </div>
                    <p class="column-header">Иван петров</p>
                    <p class="column-content">Граффический дизайнер</p>
                    <div class="social-links">
                        <a href="#"><span class="tw-icon"></span></a>
                        <a href="#"><span class="fb-icon"></span></a>
                        <a href="#"><span class="pin-icon"></span></a>
                    </div>

                </div> <!-- Column -->
            </div><!-- Row Section 4 -->
            <p class="slogan-team">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, perferendis, labore, unde, quaerat quam esse quae officiis explicabo ex odio accusantium qui obcaecati iste debitis ab doloribus sint nobis commodi.</p>
        </div><!-- Inner Wrapper -->
     </div>
</div>
<div class="load">
    <ul class="loader">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>