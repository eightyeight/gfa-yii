<?php
/* @var $this SiteController */

?>




<section id="banner">
    <header id="header" style="z-index: 200">
<!--        <div class="row alt-menu" >-->

<!--        </div>-->

        <div class="row"  >
            <div class="alt-logo">

                <img src="images/logo-white.png" >

            </div>

            <div class="slogan">
                <strong>Ваша эффективная реклама</strong>
            </div>
            <div class="phone-desc" >
                <div>
                    <div>Есть вопрос?</br> Звони - поможем! </div>
                    <span></span>
                    <div>8(495) 215-19-22</div>
                </div>
            </div>
        </div>

        <nav id="nav" class="main">
            <?php $this->widget('zii.widgets.CMenu',array(
                'items'=>array(
                    array('template'=>'<a href="/"><i class="fa fa-home" style="font-size: 1.5em"></i></a>', 'url'=>array('site/index'), 'itemOptions' => array('class' => 'navItem')),
                    array('label'=>'Портфолио', 'url'=>array('site/portfolio'), 'itemOptions' => array('class' => 'navItem')),
                    array('label'=>'Услуги', 'url'=>array('site/services'), 'itemOptions' => array('class' => 'navItem')),
                    array('label'=>'Кто мы', 'url'=>array('/about/index'), 'itemOptions' => array('class' => 'navItem'),
                        'submenuOptions'=>array('class'=>'nav-sub'),
                        'items'=>array(
                            array('label'=>'Новости', 'url'=>array('about/news')),
                            array('label'=>'Команда', 'url'=>array('about/team')),
                            array('label'=>'Отзывы', 'url'=>array('about/review')),
                        ),
                    ),
                    array('label'=>'Вакансии', 'url'=>array('/site/vacancy'), 'itemOptions' => array('class' => 'navItem')),
                    array('label'=>'Контакты', 'url'=>array('/site/contact'), 'itemOptions' => array('class' => 'navItem')),
                    array('itemOptions' => array('id' => 'pointer', 'class' => 'pointer')),
                    array('template'=>'<span class="call-button">Заказать звонок</span>','itemOptions' => array('id' => 'call-button')),
                ),
                'htmlOptions'=>array(
                    'class' => 'navInner'
                )
            )); ?>


        </nav>


    </header>
    <div id="top-slider">
        <div class="slide" style=""><div>Комплексный подход в решении рекламных задач </div> <span class="slide-inner">от проектирования до фактического результата</span></div>
        <div class="slide" style=""><div>Действительно заметная наружная реклама </div> <span class="slide-inner">яркая, нестандартная, привлекающая клиентов</span></div>
        <div class="slide" style=""><div>Команда умеющая работать на результат </div> <span class="slide-inner">наш результат - это Ваша прибыль</span></div>
    </div>
    <!-- Let it snow! -->
    <div class="xms">
        <canvas id="xmas"></canvas>
    </div>

</section>
<!-- Main -->
<article id="main" >

    <!-- Company Cases -->
    <section class="wrapper  special container ">

        <div class="row" style="padding: 2em; min-height: 319px;" id="services" >
            <div class="3u" style="display: none">
                <header class="service">
                    <a href="http://lp.gf-a.ru" class="feature"><img src="images/icon1-1.png" alt="" style="height: 100px;"/></a>
                    <h3>
                        <span>Наружная</span>
                        <span>Реклама </span>
                    </h3>
                </header>
                <div style="clear: both"></div>
                <div>
                    <p>Производство, монтаж и регистрация</p>
                </div>
            </div>
            <div class="3u" style="display: none">
                <header class="service">
                    <a href="#" class="feature"><img src="images/icon1-2.png" alt="" style="height: 100px;"/></a>

                    <h3>
                        <span>Дизайн</span>
                        <span>Студия</span>

                    </h3>
                </header>
                <div style="clear: both"></div>
                <div>
                    <p>Нестандартные подходы в наружной и интерьерной рекламы</p>
                </div>
            </div>
            <div class="3u" style="display: none">
                <header class="service">
                    <a href="#" class="feature"><img src="images/icon1-3.png" alt="" style="height: 100px;"/></a>
                        <h3>
                            <span>Рекламные </span>
                            <span>Конструкции</span>
                        </h3>
                </header>
                <div style="clear: both"></div>
                <div>
                    <p>Обширная база рекламных мест различного формата</p>
                </div>

            </div>
            <div class="3u" style="display: none" >
                <header class="service">
                    <a href="#" class="feature"><img src="images/icon1-4.png" alt="" style="height: 100px;"/></a>

                        <h3>
                            <span>Web </span>
                            <span>Cтудия</span>
                        </h3>
                </header>
                <div style="clear: both"></div>
                <div>
                    <p>Разработка сайтов любой сложности</p>
                </div>

            </div>
        </div>
    </section>
    <div class="container big">
        <hr class="separator">
    </div>

    <!-- Clients -->
    <section class="wrapper special container clients">
        <header class="container">
            <div class="">

                <div class=""><h2>Наши Клиенты</h2></div>

            </div>

        </header>
        <div style="display: flex;">
            <div class="slider-prev" style="margin: auto;"></div>
            <div class="bxslider">
                <div id="cl-slide">
                    <div class="row">
                        <img src="/images/client%20(7).png" class="3u"/>
                        <img src="/images/client%20(2).png" class="3u"/>
                        <img src="/images/client%20(9).png" class="3u"/>
                        <img src="/images/client%20(10).png" class="3u"/>
                    </div>
                    <div class="row">
                        <img src="/images/client%20(5).png" class="3u"/>
                        <img src="/images/client%20(12).png" class="3u"/>
                        <img src="/images/rusavto.png" class="3u"/>
                        <img src="/images/client%20(8).png" class="3u"/>
                    </div>
                </div>

                <div id="cl-slide">
                    <div class="row">
                        <img src="/images/client%20(5).png" class="3u"/>
                        <img src="/images/client%20(12).png" class="3u"/>
                        <img src="/images/client%20(1).png" class="3u"/>
                        <img src="/images/client%20(8).png" class="3u"/>
                    </div>
                    <div class="row">
                        <img src="/images/client%20(3).png" class="3u"/>
                        <img src="/images/client%20(4).png" class="3u"/>
                        <img src="/images/client%20(11).png" class="3u"/>
                        <img src="/images/client%20(6).png" class="3u"/>
                    </div>
                </div>
            </div>
            <div class="slider-next " style="margin: auto;"></div>
        </div>
    </section>

    <!-- Two -->
    <section class="wrapper style1 special" id="whyus">

        <div class="container skew-inner">
            <header>
                <h2 style="color: #1d1d1d">Наши Преимущества</h2>
            </header>
            <div class="row ">
                <div class="4u">

                    <section class="company-info">

                        <header>
                            <img src="images/design.svg">
                            <h3>
                                <span>ДИЗАЙН МАКЕТ</span>
                                <span>В 3-Х ЦЕНОВЫХ ВАРИАНТАХ</span>
                            </h3>
                        </header>
                        <p>Предоставим несколько вариантов дизайн-макета с различными технологиями изготовления вывески в 3-ёх ценовых категориях на выбор: «VIP», «Стандарт» и «Эконом».</p>

                    </section>

                </div>
                <div class="4u">

                    <section class="company-info">

                        <header>
                            <img src="images/info.svg">
                            <h3>
                                <span>СОБСТВЕННОЕ</span>
                                <span>ПРОИЗВОДСТВО</span>
                            </h3>
                        </header>
                        <p>Собственное производство, профессиональные сотрудники и большой опыт работы гарантируют выполнение заказа точно в срок. С нами вы не будете терять время.</p>
                    </section>

                </div>
                <div class="4u">

                    <section class="company-info">

                        <header>
                            <img src="images/worker.svg">
                            <h3>
                                <span>БЕСПЛАТНЫЙ</span>
                                <span>ВЫЕЗД СПЕЦИАЛИСТА</span>
                            </h3>
                        </header>
                        <p>Мы бесплатно приедем, сделаем замер, составим подробное задание.</p>

                    </section>

                </div>
            </div>
        </div>
    </section>

    <!-- Three -->


</article>

<!-- CTA -->
<section id="cta">
    <div class="promo-inner">
        <header>
            <h2> <strong>ЗАКАЖИТЕ ПРОФТЕХРАСЧЕТ
                    СТОИМОСТИ ВЫВЕСКИ СЕГОДНЯ</strong></h2>
            <p>и мы бесплатно зарегистрируем и согласуем
                её установку в администрации города</p>
        </header>
        <footer>
            <div id="promo-success" ></div>
            <?php $form = $this->beginWidget('CActiveForm',
                array(
                    'id' => 'promo-form',
                    'enableAjaxValidation'=>true,
                    'enableClientValidation'=>true,
                    'action'=> CHtml::normalizeUrl(array('sendmail/send')),
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                        'validateOnChange'=>false,
                    ),
                )
            ) ?>
                <input type="hidden" name="className" value="<?php echo get_class($promo)?>">
                <?php echo $form->textField($promo,'name', array('placeholder'=>'Введите Имя', 'class'=>'promo-input', 'id' => 'name')); ?>
                <?php echo $form->error($promo,'name'); ?>
                <?php echo $form->textField($promo,'phone', array('placeholder'=>'Введите Телефон', 'class'=>'promo-input', 'id' => 'phone')); ?>
                <?php echo $form->error($promo,'phone'); ?>
                <?php echo CHtml::ajaxSubmitButton('Написать', $this->createUrl('sendmail/send'),
                    array(
                        'type'=>'POST',
                        'success'=>'function(data){
                                                console.log(data);
                                                var data = jQuery.parseJSON(data);

                                                if(data.status=="success"){

                                                    alert("Success");

                                                }
                                                else{
                                                        jQuery.each(data, function(key, value) {
                                                            jQuery("#"+key+"_em_").show().html(value.toString());
                                                        });
                                                }
                                            }'
                    ),
                    array(
                        'class' => 'button special',
                    )
                );?>
            <?php $this->endWidget(); ?>
        </footer>
    </div>
</section>
<!-- Map -->
<section >


    <div id="map"></div>


</section>