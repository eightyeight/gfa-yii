<?php
Yii::app()->clientScript->registerScriptFile('/js/jquery.jswipe-0.1.2.js');
Yii::app()->clientScript->registerScriptFile('/js/portfolio.js');
Yii::app()->clientScript->registerCssFile('/css/portfolio.css');


?>

<div class="wrapper">
    <button id="leftButton" onclick='moveLeft()'>&lt;</button>
    <button id="rightButton" onclick='moveRight()'>&gt;</button>
    <div id="galleryContainer">
        <div id="gallery">
            <?php foreach($data as $project):?>
                <img src="/uploads/portfolio/<?php echo $project->preview_image?>" alt="<?php echo $project->project_name?>" />
            <?php endforeach;?>
        </div>
        <div id="caption">Photo Caption</div>
        <div id="loading">Загружаемся...</div>
    </div>
</div>

