<?php

class SiteController extends FrontendController
{



	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(

			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xfb993f,
                'foreColor'=>0xffffff,
			),

			'job'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function actionLogin() {

        $serviceName = Yii::app()->request->getQuery('service');
        if (isset($serviceName)) {
            /** @var $eauth EAuthServiceBase */
            $eauth = Yii::app()->eauth->getIdentity($serviceName);
            $eauth->redirectUrl = Yii::app()->user->returnUrl;
            $eauth->cancelUrl = $this->createAbsoluteUrl('site/login');

            try {
                if ($eauth->authenticate()) {
                    //var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
                    $identity = new UserIdentity($eauth);

                    // successful authentication
                    if ($identity->authenticate()) {
                        Yii::app()->user->login($identity);
                        $session = Yii::app()->session;
                        $session['eauth_profile'] = $eauth->attributes;

                        // special redirect with closing popup window
                        $eauth->redirect('/about/review');
                    }
                    else {
                        // close popup window and redirect to cancelUrl
                        $eauth->cancel();
                    }
                }

                // Something went wrong, redirect to login page
                $this->redirect(array('about/review'));
            }
            catch (EAuthException $e) {
                // save authentication error to session
                Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
                $eauth->redirect('/');
            }
        }


    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $this->layout = 'site';
        $phone = new PhoneForm;
        $promo = new PromoForm;

		$this->render('index', array('phone' => $phone, 'promo' => $promo));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    /**
     * Displays the contact page
     */
    public function actionContact()
    {

        $model=new ContactForm;
        /**
         * Adding JS to Contacts Page
         */

        /**
         * @TODO remove this from controller
         */
        Yii::app()->clientScript
            ->registerScriptFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false',CClientScript::POS_HEAD)
            ->registerScript('map',"google.maps.event.addDomListener(window, 'load', init);
                function init() {
                    var mapOptions = {
                        scrollwheel: false,
                                    navigationControl: false,
                                    mapTypeControl: false,
                                    scaleControl: false,
                                    draggable: false,
                                    zoom: 15,
                                    disableDefaultUI: true,
                                    center: new google.maps.LatLng(56.179924, 37.003441),
                                    styles: [{'featureType':'water','elementType':'geometry','stylers':[{'color':'#000000'},{'lightness':17}]},{'featureType':'landscape','elementType':'geometry','stylers':[{'color':'#000000'},{'lightness':20}]},{'featureType':'road.highway','elementType':'geometry.fill','stylers':[{'color':'#000000'},{'lightness':17}]},{'featureType':'road.highway','elementType':'geometry.stroke','stylers':[{'color':'#000000'},{'lightness':29},{'weight':0.2}]},{'featureType':'road.arterial','elementType':'geometry','stylers':[{'color':'#000000'},{'lightness':18}]},{'featureType':'road.local','elementType':'geometry','stylers':[{'color':'#000000'},{'lightness':16}]},{'featureType':'poi','elementType':'geometry','stylers':[{'color':'#000000'},{'lightness':21}]},{'elementType':'labels.text.stroke','stylers':[{'visibility':'on'},{'color':'#000000'},{'lightness':16}]},{'elementType':'labels.text.fill','stylers':[{'saturation':36},{'color':'#000000'},{'lightness':40}]},{'elementType':'labels.icon','stylers':[{'visibility':'off'}]},{'featureType':'transit','elementType':'geometry','stylers':[{'color':'#000000'},{'lightness':19}]},{'featureType':'administrative','elementType':'geometry.fill','stylers':[{'color':'#000000'},{'lightness':20}]},{'featureType':'administrative','elementType':'geometry.stroke','stylers':[{'color':'#000000'},{'lightness':17},{'weight':1.2}]}]
                                };

                                var mapElement = document.getElementById('map');

                                var map = new google.maps.Map(mapElement, mapOptions);

                                marker = new google.maps.Marker({
                                    map:map,
                                    // draggable:true,
                                    // animation: google.maps.Animation.DROP,
                                    position: new google.maps.LatLng(56.17970, 37.00359),
                                    icon: 'http://gf-a.ru/marker.png' // null = default icon
                                });
                    }", CClientScript::POS_END)
        ;

        /**
         * If Request was from Main Page.
         */


        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $amocrm = Yii::app()->amocrm;
                $amocrm->addFormData($model);
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Все удачно отправилось');
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model,));
    }




    public function actionTeam()
    {

        $this->render('team');
    }


    public function actionVacancy()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/vendor/EasePack.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/vendor/TweenLite.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/anim-header.js', CClientScript::POS_END);

        $jobs = Job::model()->findAll();

        $this->render('vacancy', array('jobs' => $jobs));
    }

    public function actionJob($id)
    {
        $model=Job::model()->findByPk($id);

        $this->render('view', array('model' => $model));


    }

    public function actionUnderconstruct()
    {
        $this->render('underconstruct');
    }

    public function actionPortfolio()
    {

        $data = Project::model()->findAll();
        $this->render('portfolio', array('data' => $data));

    }

    public function actionServices()
    {

        $this->render('services');
    }

    public function actionConst(){
        $this->render('const');
    }

    public function actionTest(){

        $megaplan = new MegaplanAPI();
        $megaplan->authenticate();
        $megaplan->getEmployeeList();

        print_r($megaplan->getDate());

    }
}