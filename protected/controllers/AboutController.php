<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 24.11.14
 * Time: 14:15
 */

class AboutController extends FrontendController{


    public function actionIndex(){
        $this->render('index');
    }

    public function actionNews(){
        Yii::import('backend.protected.modules.news.models.News');
        $dataProvider = new CActiveDataProvider('News', array(
            'criteria'=>array(
                'scopes'=>array('published'),
                'order'=>'created_time DESC',
            ),
            'pagination'=>array(
                'pageSize'=>10,
            ),
        ));

        $this->render('news', array('dataProvider' => $dataProvider));
    }

    public function actionTeam(){
        $this->render('team');
    }

    public function actionReview(){
        $this->render('review');
    }

    public function actionNewsView($id){
        Yii::import('backend.protected.modules.news.models.News');
        $model = News::model()->findByPk((int)$id);
        $this->render('newsview', array('data' => $model));
    }
} 