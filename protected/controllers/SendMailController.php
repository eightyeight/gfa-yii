<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 07.11.14
 * Time: 12:15
 */

Yii::import('vendor.swiftmailer.swiftmailer.lib.swift_required', true);

class SendMailController extends CController {

    public $request;

    public function init()
    {
        $this->request = Yii::app()->request;
    }

    public function actionSend(){

        if(Yii::app()->request->isAjaxRequest){

            $factory = FormModelFactory::getInstance();

            $factory->setRequest($this->request);

            $model = $factory->createModel();

            $searchEngine = null;

            $searchQuery = null;

            if($model->validate()){
                     echo CJSON::encode(array(
                           'status'=>'success'
                        ));


                try{

                    $amocrm = Yii::app()->amocrm;

                    $amocrm->addFormData($model);

                }
                catch(Exeption $e){

                    echo "amocrm not sync!";

                }

                try{

                    if($this->sendHtmlMail($model, $searchEngine, $searchQuery)){
                        echo CJSON::encode(array(
                            'status'=>'success'
                        ));
                    }
                    Yii::app()->end();

                }

                catch(Swift_TransportException $e){

                    echo 'Error msg:';

                    echo $e->getMessage();

                }
            }
            else{

                $error = CActiveForm::validate($model);

                if($error!='[]')
                    echo $error;

                Yii::app()->end();
            }

        }
    }


    public function sendHtmlMail(CFormModel $model ,$searchEngine, $searchQuery) {



        $subject = Yii::app()->request->hostinfo .': Получен новый лид.';

        $body = '<html>' .
            ' <head></head>' .
            '<body>'.
            '<p>
            Здравствуйте
            </p>

            <p>
            Получен новый лид с целевой страницы: '. Yii::app()->request->hostinfo .'
				</p>

				<p>
				Данные лида:
				</p>
                <p>
                Cайт: "'.Yii::app()->request->hostinfo.'"
                </p>
                <p>
                Поисковая ситема: "'.$searchEngine.'"
                </p>
                <p>
                Запрос: "'.$searchQuery.'"
                </p>

				<p>
				Имя: "'. $model->name .'"
				</p>

				<p>
				Телефон: "'. $model->phone .'"
                </p>

				<p>
				Почта: "' . $model->email . '"
				</p>

				<p>
				--
				Это автоматическое сообщение, отвечать на него не нужно.
				</p>
				'.
            '</body>' .
            '</html>';

        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('noreply@gf-a.ru')
            ->setTo('bodrov@gf-a.ru')
            ->setBody($body, 'text/html');



        $transport = Swift_SmtpTransport::newInstance();

        $mailer = Swift_Mailer::newInstance($transport);

        return $mailer->send($message);
    }



}