<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 29.08.14
 * Time: 10:37
 */

class PaymentController extends FrontendController
{

    public $layout='//layouts/payment';

    public function actionIndex() {

        //jQuery UI
        Yii::app()->clientScript
            ->registerScript('tooltipInit',"
              $(function() {
                $( document ).tooltip();
              });
            ", CClientScript::POS_END);

        // Выставляем счет
        $invoice = new Invoice;

        if (isset($_POST['Invoice'])) {

            $invoice->attributes = $_POST['Invoice'];

            $invoice->created_at = new CDbExpression('NOW()');

            if ($invoice->save()) {

                // Компонент переадресует пользователя в свой интерфейс оплаты
                Yii::app()->robokassa->pay(
                    $invoice->amount,
                    $invoice->id,
                    $invoice->description,
                    $invoice->email
                );

            }
        }
        $this->render('index',array('invoice' => $invoice));
    }

    /*
        К этому методу обращается робокасса после завершения интерактива
        с пользователем. Это может произойти мгновенно либо в течение нескольких
        минут. Здесь следует отметить счет как оплаченный либо обработать
        отказ от оплаты.
    */
    public function actionResult() {
        $rc = Yii::app()->robokassa;
        print_r(Yii::app()->user->getFlash('global'));
        // Коллбэк для события "оплата произведена"
        $rc->onSuccess = function($event){
            $transaction = Yii::app()->db->beginTransaction();
            // Отмечаем время оплаты счета
            $InvId = Yii::app()->request->getParam('InvId');
            $invoice = Invoice::model()->findByPk($InvId);
            $invoice->paid_at = new CDbExpression('NOW()');
            if (!$invoice->save()) {
                $transaction->rollback();
                throw new CException("Unable to mark Invoice #$InvId as paid.\n"
                    . CJSON::encode($invoice->getErrors()));
            }
            $transaction->commit();

        };

//         Коллбэк для события "отказ от оплаты"
        $rc->onFail = function($event){
            // Например, удаляем счет из базы
            $InvId = Yii::app()->request->getParam('InvId');
            Invoice::model()->findByPk($InvId)->delete();

        };

//         Обработка ответа робокассы
        $rc->result();
    }

    /*
        Сюда из робокассы редиректится пользователь
        в случае отказа от оплаты счета.
    */
    public function actionFailure() {
        Yii::app()->user->setFlash('global', 'Отказ от оплаты. Если вы столкнулись
            с трудностями при внесении средств на счет, свяжитесь
            с нашей технической поддержкой.');

        $this->redirect('index');
    }

    /*
        Сюда из робокассы редиректится пользователь в случае успешного проведения
        платежа. Обратите внимание, что на этот момент робокасса возможно еще
        не обратилась к методу actionResult() и нам неизвестно, поступили средства
        на счет или нет.
    */
    public function actionSuccess() {
        $InvId = Yii::app()->request->getParam('InvId');
        $invoice = Invoice::model()->findByPk($InvId);
        if ($invoice) {
            Yii::app()->user->setFlash('global',
                'Средства зачислены на ваш личный счет. Спасибо.');
        }
        $this->redirect('index');
    }
} 