<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 28.08.14
 * Time: 12:21
 */

class Invoice extends CActiveRecord
{
    public function tableName() {
        return '{{invoice}}';
    }

    public function attributeLabels()
    {
        return array(

             'id' => 'ID',
             'amount' => 'Сумма Платежа',
             'description' => 'Описание Платежа',
             'email' => 'Почта Плательщика',
             'created_at' => 'Дата Создания Платежа',
             'paid_at'  => 'Дата Платежа',

        );
    }

    public function rules() {
        return array(
            array('amount, description, email, typeId', 'required'),
            array('email', 'email'),
            array('amount', 'numerical'),
            array('description', 'length', 'max'=>200),
        );
    }

    public function relations(){
        return array(
            'service_type' => array(self::HAS_ONE, 'ServiceType', 'id')
        );
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

}