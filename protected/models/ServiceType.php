<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 23.09.14
 * Time: 14:21
 */

class ServiceType extends CActiveRecord {

    public function tableName() {
        return '{{service_type}}';
    }

    public function relations(){
        return array(
            'service_type' => array(self::HAS_MANY, 'Invoice', 'typeId')
        );
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

} 