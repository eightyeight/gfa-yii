<div class="services">
	<ul class="auth-services clear">
		<?php
		foreach ($services as $name => $service) {
			echo '<li class="auth-service ' . $service->id . '">';
            if($service->id == 'vkontakte')
                $html = '<span class="auth-icon"><i class="fa fa-vk"></i></span>';
            elseif($service->id == 'facebook')
                $html = '<span class="auth-icon"><i class="fa fa-facebook"></i></span>';
			$html = CHtml::link($html, array($action, 'service' => $name), array(
				'class' => 'auth-link ' . $service->id,
			));
			echo $html;
			echo '</li>';
		}
		?>
	</ul>
</div>
