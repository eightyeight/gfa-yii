<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 17.11.14
 * Time: 15:28
 */

class VacanciesWidget extends CWidget{

    private $data = null;

    public function init(){

        $hhAPI = new HeadHunterAPI();

        if(!$this->data)
            $this->data = $hhAPI->getVacanciesData();

        parent::init();
    }

    public function run(){
        if(!$this->data)
            $this->render('novacancies');
        $this->render('view', array('data' => $this->data));

    }

} 