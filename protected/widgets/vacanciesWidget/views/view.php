<?php

echo CHtml::openTag('div');
    foreach($data as $vacancy):
        echo CHtml::openTag('div');

            echo CHtml::openTag('input',array('type' => 'checkbox', 'class' =>'questions', 'id' => $vacancy['id']));
            echo CHtml::openTag('div',array('class' => 'plus'));
            echo '+';
            echo CHtml::closeTag('div');
            echo CHtml::openTag('label',array('class' => 'question', 'for' => $vacancy['id']));
                       echo $vacancy['name'];
            echo CHtml::closeTag('input');
            echo Chtml::closeTag('label');
            echo CHtml::openTag('div',array('class' => 'answers'));

                echo CHtml::openTag('div',array('class' => 'head-vacancy'));
                    echo CHtml::openTag('ul',array());

                         echo Chtml::openTag('li');
                            echo Chtml::openTag('label');
                            echo 'Уровень зарплаты';
                            echo Chtml::closeTag('label');
                            if(!$vacancy['salary']['from'] && !$vacancy['salary']['to']):
                                echo CHtml::openTag('div');
                                echo 'Обсуждается по результатам собеседования.';
                                echo CHtml::closeTag('div');
                            else:
                                echo CHtml::openTag('div');
                                echo 'От ' . $vacancy['salary']['from'] . ' до ' .$vacancy['salary']['to']. ' руб.';
                                echo CHtml::closeTag('div');
                            endif;
                         echo Chtml::closeTag('li');

                         echo Chtml::openTag('li');
                            echo Chtml::openTag('label');
                            echo 'Требуемый опыт работы';
                            echo Chtml::closeTag('label');
                            echo CHtml::openTag('div');
                            echo $vacancy['experience']['name'];
                            echo CHtml::closeTag('div');
                         echo Chtml::closeTag('li');

                    echo Chtml::closeTag('ul');
                echo CHtml::closeTag('div');

                echo CHtml::openTag('h3');
                    echo 'Описание вакансии:';
                echo CHtml::closeTag('h3');

                echo CHtml::openTag('div');
                    echo $vacancy['description'];
                echo CHtml::closeTag('div');

            echo CHtml::closeTag('div');

        echo CHtml::closeTag('div');
    endforeach;
echo CHtml::closeTag('div');