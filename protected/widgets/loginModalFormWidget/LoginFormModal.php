<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 08.12.14
 * Time: 12:49
 */


class LoginFormModal extends CWidget {

    public $form_model;

    public function init(){
        Yii::app()->clientScript->registerScriptFile('/js/loginform.js');
        if(!isset($this->form_model))
            $this->form_model = new LoginForm();
    }

    public function run(){
      echo Chtml::openTag('div', array('class' => 'loginHeaderWrapper'));
        echo CHtml::link('Вход',' ', array('id' => 'loginLink')) . ' / ' . Chtml::link('Регистрация',' ',array('id' => 'registerLink'));
      echo Chtml::closeTag('div');
      $this->renderForm();

    }


    public function renderForm(){
        echo CHtml::openTag('div', array('id' => 'loginFromWrapper'));
            echo CHtml::openTag('p');
                echo 'Введите свою почту и пароль';
            echo Chtml::closeTag('p');
            $form = $this->getController()->beginWidget('CActiveForm',array(
                'id' => 'login-form',
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'action'=> CHtml::normalizeUrl(array('site/login')),
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                    'validateOnChange'=>false,
                ),
                'htmlOptions' => array(
                    'class' => 'login',
                )
            ));

            echo $form->textField($this->form_model,'email', array('placeholder'=>'Ваш e-mail', 'class'=>'promo-input', 'id' => 'email'));
            echo $form->error($this->form_model,'email', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));
            echo $form->passwordField($this->form_model,'password', array('placeholder'=>'Ваш пароль', 'class'=>'promo-input', 'id' => 'password'));
            echo $form->error($this->form_model,'password', array('style' => 'color: rgb(255, 88, 2); font-size: 12px; text-align: center;'));

            $this->getController()->endWidget();
            echo CHtml::openTag('p');
                echo 'Либо войдите через аккаунт в соцсетях';
            echo Chtml::closeTag('p');
            $this->getController()->widget('ext.eauth.EAuthWidget', array('action' => 'site/login'));

        echo Chtml::closeTag('div');
    }

} 