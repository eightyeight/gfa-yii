<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 09.12.14
 * Time: 10:55
 */




class UserInfoWidget extends CWidget{

    public $user;

    public function init(){
        if(!$this->user)
          $this->user = Yii::app()->getModule('user')->user();

    }

    public function run(){
        $this->renderInfo();
    }

    public function renderInfo(){
        echo CHtml::opentag('div', array('class' => 'userInfo'));
            echo Chtml::openTag('span');
                echo $this->user->profile->firstname . ' ' . $this->user->profile->lastname;
            echo CHtml::closeTag('span');
            echo Chtml::openTag('div', array('class' => 'userPic'));
            echo Chtml::image($this->user->profile->userpic);
            echo CHtml::closeTag('div');
            echo Chtml::link(Yii::t('app','Logout'),'/site/logout');
        echo CHtml::closetag('div');
    }
} 