<div class="newsItem">
    <div class="newsdate"><?php echo $data->created_time ?></div>
    <h1><?php echo $data->title ?></h1>

    <p><?php echo $data->preview_text ?></p>
    <div class="clearfix"></div>
    <div class="more"><?php echo CHtml::link('Подробнее...',Yii::app()->createUrl('about/newsview', array('id' => $data->id)),array(
            'class' => 'button small',
        ))?>
    </div>
    <div class="clearfix"></div>

</div>
<hr>