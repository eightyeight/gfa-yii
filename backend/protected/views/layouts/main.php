<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="ru">



	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
        <?php
        $this->widget(
        'bootstrap.widgets.TbNavbar',
            array(
                'type' => 'inverse',
                'fixed' => 'top',
                'fluid' => false,
                'items' => array(
                                array(
                                    'class' => 'bootstrap.widgets.TbMenu',
                                    'type' => 'navbar',
                                    'items'=>array(
                                        array('label'=>'Новости', 'url'=>Yii::app()->createUrl('news')),
                                        array('label'=>'Пользователи', 'url'=> Yii::app()->createUrl('user')),
                                        array('label'=>'Разграничение доступа', 'url'=>Yii::app()->createUrl('rights')),
                                        )
                                    ),
                                array(
                                         'class' => 'bootstrap.widgets.TbMenu',
                                         'type' => 'navbar',
                                         'htmlOptions' => array(
                                             'class' => 'pull-right'
                                         ),
                                         'items' => array(
                                             array('label'=>'Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
                                             array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                                         )
                                     )
                                )

                            )
                );
        ?>

	</div>
    <hr>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
    <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'fade' => true,
            'closeText' => '&times;', // false equals no close link
            'events' => array(),
            'htmlOptions' => array(),
            'userComponentId' => 'user',
            'alerts' => array( // configurations per alert type
                // success, info, warning, error or danger
                'success' => array('closeText' => '&times;'),
                'info', // you don't need to specify full config
                'warning' => array('closeText' => false),
                'error' => array('closeText' => '&times;')
            ),
        ));
    ?>
    <hr>
	<?php echo $content; ?>

	<div class="clear"></div>



</div><!-- page -->
<div class="container">
    <?php include_once('footer.php');?>
</div>


</body>
</html>
