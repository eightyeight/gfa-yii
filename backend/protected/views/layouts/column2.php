<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="span2">
        <div id="sidebar">
            <?php
            $this->widget('bootstrap.widgets.TbMenu', array(
                'type' => 'pills',
                'items'=>$this->menu,
            ));
            ?>
        </div>
    </div>

    <div class="span10">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>

</div>
<?php $this->endContent(); ?>