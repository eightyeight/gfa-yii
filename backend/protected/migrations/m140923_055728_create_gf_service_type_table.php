<?php

class m140923_055728_create_gf_service_type_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('gf_service_type', array(
            'id' => 'pk',
            'name' => 'string NOT NULL'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Производство'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Дизайн'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Веб-Студия'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Регистрация Рекламы'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Наружная Реклама'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Сувенирная Продукция / Типография'
        ));
        $this->insert('gf_service_type',array(
            'name' => 'Другое'
        ));
	}

	public function down()
	{
		$this->dropTable('gf_service_type');
	}


	// Use safeUp/safeDown to do migration with transaction
    /*
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
    */

}