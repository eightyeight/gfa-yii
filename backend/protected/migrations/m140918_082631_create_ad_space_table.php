<?php

class m140918_082631_create_ad_space_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('ad_space',array(
                'id' => 'pk',
                'name' => 'string NOT NULL',
                'longitude' => 'decimal NOT NULL',
                'latitude' => 'decimal NOT NULL',
                'typeId' => 'int NOT NULL',
                'address' => 'string NOT NULL',
                'created_time' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'update_time'  => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00" ON UPDATE CURRENT_TIMESTAMP',
                'available' => 'bool'
            )
        );
	}

	public function down()
	{
		$this->droptable('ad_space');
	}

}