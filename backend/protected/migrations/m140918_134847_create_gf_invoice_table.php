<?php

class m140918_134847_create_gf_invoice_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('gf_invoice', array(
                'id' => 'pk',
                'amount' => 'float NOT NULL',
                'description' => 'string NULL',
                'email' => 'string NOT NULL',
                'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'paid_at'  => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'typeId' => 'int NOT NULL'
            )
        );
	}



	public function down()
	{
		$this->dropTable('gf_invoice');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}