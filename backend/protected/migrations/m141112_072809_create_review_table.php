<?php

class m141112_072809_create_review_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('gf_review', array(
                'id' => 'pk',
                'user_name' => 'string NULL',
                'user_email' => 'string NOT NULL',
                'created_time' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'update_time'  => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'review_text' => 'text',
            )
        );
	}

	public function down()
	{

        $this->dropTable('gf_review');
		echo "m141112_072809_create_review_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}