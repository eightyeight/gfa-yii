<?php

class m141118_134907_create_news_table extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable('gf_news', array(
                'id' => 'pk',
                'user_id' => 'int NOT NULL DEFAULT 1 REFERENCES tbl_category(id)',
                'status' => 'int NOT NULL',
                'title' => 'string NOT NULL',
                'created_time' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'update_time'  => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'preview_text'=>'text',
                'news_text' => 'text',
            )
        );

        $this->addForeignKey('FK_user_id', 'gf_news', 'user_id', 'gf_users', 'id', 'RESTRICT', 'RESTRICT');
	}

	public function safeDown()
	{
        $this->dropTable('gf_news');
		echo "m141118_134907_create_news_table is down.\n";

	}

}