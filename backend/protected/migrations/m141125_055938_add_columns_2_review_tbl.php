<?php

class m141125_055938_add_columns_2_review_tbl extends CDbMigration
{


	public function safeUp()
	{
        $this->addColumn('gf_review', 'work_position', 'string NULL');
        $this->addColumn('gf_review', 'company_name', 'string NULL');
	}

	public function safeDown()
	{
        echo "m141125_055938_add_columns_2_review_tbl migration is down.\n";
	}

}