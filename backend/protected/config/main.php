<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Gfa Admin',
    'language' => 'ru',

	'preload'=>array('log','debug','bootstrap'),

    'aliases' => array(
        'vendor' => realpath(__DIR__ . '/../../vendor/'),
    ),

	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.modules.user.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.models.*',
        'application.modules.rights.components.*',
        'application.vendor.crisu83.yiistrap.helpers.TblHtml'

	),



	'modules'=>array(

        'news',
        'portfolio',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
            'generatorPaths' => array('application.vendor.crisu83.yiistrap.gii'),
			'password'=>'secret',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5',

            # send activation email
            'sendActivationMail' => true,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/user/profile'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),

        'rights' => array(

        ),
	),

	// application components
	'components'=>array(
        'debug' => array(
            'class' => 'application.modules.yii2-debug.Yii2Debug',
        ),
        'bootstrap' => array(
            'class' => 'application.vendor.clevertech.yii-booster.src.components.Bootstrap',
        ),

		'user'=>array(
            'class' => 'RWebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('user/login'),
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
                array('places/list', 'pattern'=>'api/places', 'verb'=>'GET'),
                array('places/view', 'pattern'=>'api/places/<id:\d+>', 'verb'=>'GET'),
                array('places/update', 'pattern'=>'api/places/<id:\d+>', 'verb'=>'PUT'),
                array('places/delete', 'pattern'=>'api/places/<id:\d+>', 'verb'=>'DELETE'),
                array('places/create', 'pattern'=>'api/places', 'verb'=>'POST'),
//				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
//				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
//				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

        'authManager' => array(
            'class' => 'RDbAuthManager',
            'assignmentTable' => 'gf_auth_assignment',
            'itemTable' => 'gf_authItem',
            'itemChildTable' => 'gf_authItemChild',
            'rightsTable' => 'gf_rights',
        ),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=gfa',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'rtyghbnm',
			'charset' => 'utf8',
            'enableProfiling' => true,
            'enableParamLogging' => true,
            'tablePrefix' => 'gf_'
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning,profile',
				),

			),
		),

        'messages'=>array(
            'class'=>'PhpMessageSource',
        ),


	),


	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
	),
);