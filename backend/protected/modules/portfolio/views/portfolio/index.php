<?php
/* @var $this PortfolioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    PortfolioModule::t('Projects'),
);

$this->menu=array(
	array('label'=>PortfolioModule::t('Create Projects'), 'url'=>array('create')),
	array('label'=>PortfolioModule::t('Manage Projects'), 'url'=>array('admin')),
);
?>

<h1><?= PortfolioModule::t('Projects')?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
