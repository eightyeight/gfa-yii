<?php
/* @var $this PortfolioController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>PortfolioModule::t('List Projects'), 'url'=>array('index')),
	array('label'=>PortfolioModule::t('Create Projects'), 'url'=>array('create')),
	array('label'=>PortfolioModule::t('View Projects'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>PortfolioModule::t('Manage Projects'), 'url'=>array('admin')),
);
?>

<h1> <?php echo PortfolioModule::t('Update Projects') . ' ' . $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>