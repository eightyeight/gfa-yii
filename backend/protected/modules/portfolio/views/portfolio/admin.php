<?php
/* @var $this PortfolioController */
/* @var $model Projects */

$this->breadcrumbs=array(
    PortfolioModule::t('Projects')=>array('index'),
    PortfolioModule::t('Manage Projects'),
);

$this->menu=array(
	array('label'=>PortfolioModule::t('List Projects'), 'url'=>array('index')),
	array('label'=>PortfolioModule::t('Create Projects'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#projects-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php PortfolioModule::t('Manage Projects')?></h1>




<div class="search-form" style="display:none">

</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'projects-grid',
	'dataProvider'=>$dataProvider,
    'type' => 'striped bordered condensed',
    'filter'=>$model,
	'columns'=>array(
		'id',
		'project_name',
        array(
            'class'=>'bootstrap.widgets.TbImageColumn',
            'imagePathExpression'=>'$data->getImageUrl()',
            'headerHtmlOptions'=>array('style'=>'min-width: 50px;'),
            'imageOptions'=>array('width'=>'50px'),
            'usePlaceKitten'=>false,
        ),
		'preview_text',
        array(
            'header' => Yii::t('ses', 'Edit'),
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete} {update} ',
            'viewButtonUrl' => 'Yii::app()->createUrl("portfolio/portfolio/view", array("id" => $data["id"]))',
            'updateButtonUrl' => 'Yii::app()->createUrl("portfolio/portfolio/update", array("id" => $data["id"]))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("portfolio/portfolio/delete", array("id" => $data["id"]))',
            'buttons' => array(

            )
        ),
	),
)); ?>
