<?php
/* @var $this PortfolioController */
/* @var $model Projects */

$this->breadcrumbs=array(
    PortfolioModule::t('Projects')=>array('index'),
    PortfolioModule::t('Create Projects'),
);

$this->menu=array(
	array('label'=>PortfolioModule::t('List Projects'), 'url'=>array('index')),
	array('label'=>PortfolioModule::t('Manage Projects'), 'url'=>array('admin')),
);
?>

<h1><?= PortfolioModule::t('Create Projects')?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>