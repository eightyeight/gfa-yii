<?php
/* @var $this PortfolioController */
/* @var $data Projects */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_name')); ?>:</b>
	<?php echo CHtml::encode($data->project_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preview_image')); ?>:</b>
	<img src="<?php echo $data->getImageUrl()?>">
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preview_text')); ?>:</b>
	<?php echo CHtml::encode($data->preview_text); ?>
	<br />


</div>