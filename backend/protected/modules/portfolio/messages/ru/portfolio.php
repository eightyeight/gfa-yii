<?php
return array(
  'List Projects'   => 'Список проектов в Портфолио',
  'Create Projects' => 'Создать проект',
  'Update Projects' => 'Обновить проект',
  'Delete Projects' => 'Удалить проект',
  'Manage Projects' => 'Упраление проектами',
  'Projects'        => 'Проекты Портфолио',
  'ID'              => 'ИД',
  'Project Name'    => 'Название Проекта Портфолио',
  'Preview Image'   => 'Изображение',
  'Preview Text'    => 'Описание Проекта',
  'View Projects'   => 'Просмотр проекта'
);