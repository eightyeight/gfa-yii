<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 27.11.14
 * Time: 11:56
 */

class UploadImageBehavior extends CActiveRecordBehavior{

    public $attributeName = 'preview_image';

    public $uploadDir   = 'uploads/portfolio/';

    public function beforeSave($event){
        $this->owner->{$this->attributeName} = CUploadedFile::getInstance($this->owner,$this->attributeName);
    }

    public function afterSave($event){

        //Don't be confused with that path var, it's similiar to frontend/uploads/dir. Feel free to change uploads dir.
        $path = Yii::getPathOfAlias('application').'/../../'.$this->uploadDir.$this->owner->{$this->attributeName}->getName();
        $this->owner->{$this->attributeName}->saveAs($path) ;
    }

    public function getImageUrl(){

        return 'http://'.$_SERVER['HTTP_HOST'].'/'.$this->uploadDir.'/'.$this->owner->{$this->attributeName};
    }

} 