<?php
/* @var $this DefaultController */
/* @var $model News */

$this->breadcrumbs=array(
	NewsModule::t('News')=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>NewsModule::t('List News'), 'url'=>array('index')),
	array('label'=>NewsModule::t('Create News'), 'url'=>array('create')),
	array('label'=>NewsModule::t('Update News'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>NewsModule::t('Delete News'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>NewsModule::t('Manage News'), 'url'=>array('admin')),
);
?>

<h1>View News #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'status',
		'title',
		'created_time',
		'update_time',
		'preview_text',
		'news_text',
	),
)); ?>
