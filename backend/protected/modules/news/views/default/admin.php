<?php
/* @var $this DefaultController */
/* @var $model News */

$this->breadcrumbs=array(
    NewsModule::t('News')=>array('index'),
    NewsModule::t('Manage'),
);

$this->menu=array(
	array('label'=>NewsModule::t('List News'), 'url'=>array('index')),
	array('label'=>NewsModule::t('Create News'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo NewsModule::t('Manage News')?></h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbJsonGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
    'type' => 'striped bordered condensed',
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'status',
		'title',
        array(
            'name' => 'created_time',
            'type' => 'datetime'
        ),
        array(
            'name' => 'update_time',
            'type' => 'datetime'
        ),

		/*
		'preview_text',
		'news_text',
		*/
        array(
            'header' => Yii::t('ses', 'Edit'),
            'class' => 'bootstrap.widgets.TbJsonButtonColumn',
            'template' => '{view} {delete} {update} ',
            'viewButtonUrl' => 'Yii::app()->createUrl("news/default/view", array("id" => $data["id"]))',
            'updateButtonUrl' => 'Yii::app()->createUrl("news/default/update", array("id" => $data["id"]))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("news/default/delete", array("id" => $data["id"]))',
            'buttons' => array(

            )
        ),
	),
)); ?>
