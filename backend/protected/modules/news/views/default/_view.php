<?php
/* @var $this DefaultController */
/* @var $data News */
?>

<div class="view">


	<b><?php echo CHtml::encode(NewsModule::t($data->getAttributeLabel('status'))); ?>:</b>
	<?php echo CHtml::encode(NewsModule::t($data->getStatusName())); ?>
	<br />

	<b><?php echo CHtml::encode(NewsModule::t($data->getAttributeLabel('title'))); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode(NewsModule::t($data->getAttributeLabel('preview_text'))); ?>:</b>
	<?php echo CHtml::encode($data->preview_text); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('news_text')); ?>:</b>
	<?php echo CHtml::encode($data->news_text); ?>
	<br />

	*/ ?>

</div>
<hr>