<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    NewsModule::t('News'),
);

$this->menu=array(
	array('label'=>NewsModule::t('Create News'), 'url'=>array('create')),
	array('label'=>NewsModule::t('Manage News'), 'url'=>array('admin')),
);
?>

<h1><?php echo NewsModule::t('News')?></h1>
<div width="400px">
    <?php $this->widget('bootstrap.widgets.TbListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_view',
    )); ?>
</div>