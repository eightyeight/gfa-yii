<?php
/* @var $this DefaultController */
/* @var $model News */

$this->breadcrumbs=array(
	NewsModule::t('News')=>array('index'),
	NewsModule::t('Create'),
);

$this->menu=array(
	array('label'=>NewsModule::t('List News'), 'url'=>array('index')),
	array('label'=>NewsModule::t('Manage News'), 'url'=>array('admin')),
);
?>

<h1>Create News</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>