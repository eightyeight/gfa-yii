<?php
/* @var $this DefaultController */
/* @var $model News */

$this->breadcrumbs=array(
	NewsModule::t('News')=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	NewsModule::t('Update'),
);

$this->menu=array(
	array('label'=>'List News', 'url'=>array('index')),
	array('label'=>'Create News', 'url'=>array('create')),
	array('label'=>'View News', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage News', 'url'=>array('admin')),
);
?>

<h1>Update News <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>