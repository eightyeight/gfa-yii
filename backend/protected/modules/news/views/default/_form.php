<?php
/* @var $this DefaultController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'news-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div >
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',News::getStatusArray()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div >
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div >
		<?php echo $form->labelEx($model,'preview_text'); ?>
		<?php echo $form->textArea($model,'preview_text',array('rows'=>6, 'cols'=>50, 'style' => 'width: 98%')); ?>
		<?php echo $form->error($model,'preview_text'); ?>
	</div>

	<div >
        <?php echo $form->ckEditorRow(
            $model,
            'news_text',
            array(
                'editorOptions' => array(
                    'fullpage' => 'js:true',
                    /* 'width' => '640', */
                    /* 'resize_maxWidth' => '640', */
                    /* 'resize_minWidth' => '320'*/
                )
            )
        ); ?>
		<?php echo $form->error($model,'news_text'); ?>
	</div>
    <hr>
	<div>
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'reset', 'label' => 'Reset')
        ); ?>

	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->