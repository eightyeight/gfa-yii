<?php

/**
 * This is the model class for table "{{news}}".
 *
 * The followings are the available columns in table '{{news}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property string $title
 * @property string $created_time
 * @property string $update_time
 * @property string $preview_text
 * @property string $news_text
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class News extends CActiveRecord
{

    const _DRAFT = 1;
    const _PUBLISHED = 2;
    const _ARCHIVED = 3;

    public function getStatusName(){
        $statusArray = self::getStatusArray();
        return $statusArray[$this->status];
    }

    public static function getStatusArray(){

        return array(
            self::_DRAFT     => 'Draft',
            self::_PUBLISHED => 'Published',
            self::_ARCHIVED  => 'Archived'
        );

    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{news}}';
	}

    public function behaviors(){
        $parents_behaviors = parent::behaviors();
        $self_behaviors = array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_time',
                'updateAttribute' => 'update_time',
            )
        );

        return array_merge($parents_behaviors, $self_behaviors);
    }

    protected function beforeSave(){

        if($this->isNewRecord)
            $this->user_id = Yii::app()->user->id;
        return parent::beforeSave();
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('user_id, status', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('created_time, update_time, preview_text, news_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, status, title, created_time, update_time, preview_text, news_text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'status' => 'Status',
			'title' => 'Title',
			'created_time' => 'Created Time',
			'update_time' => 'Update Time',
			'preview_text' => 'Preview Text',
			'news_text' => 'News Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('preview_text',$this->preview_text,true);
		$criteria->compare('news_text',$this->news_text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function scopes(){
        return array(
            'published'=>array(
                'condition'=>'status='. self::_PUBLISHED,
            ),
        );
    }
}
