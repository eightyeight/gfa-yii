<?php

return array(
    'News' => 'Новости',
    'Update' => 'Обновить',
    'Create' => 'Создать',
    'Create News' => 'Создать Новость',
    'Manage News' => 'Управление Новостями',
    'List News'   => 'Список Новостей',
    'Update News' => 'Обновить Новость',
    'Delete News' => 'Удалить Новость',
    '<strong>Nice one!</strong> News is succesfully added.' => '<strong>Отлично!</strong> Новая новость успешно обновленна.',
    'Draft' => 'Чернович',
    'Published' => 'Опубликованно',
    'Archived' => 'В архиве',
    'ID' => 'Идентификатор',
    'User' => 'Пользователь',
    'Status' => 'Статус',
    'Title' => 'Заголовок',
    'Created Time' => 'Время создания',
    'Update Time' => 'Время обновления',
    'Preview Text' => 'Текст превью',
    'News Text' => 'Текст новости',
);

